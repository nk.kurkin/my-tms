import random

"""1. Быки и коровы:
В классическом варианте игра рассчитана на двух игроков. Каждый из игроков
задумывает и записывает тайное 4-значное число с неповторяющимися цифрами.
Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое
противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения с
их позициями в тайном числе (то есть количество коров) и сколько угадано
вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой, пока не
отгадает всю последовательность.
Ваша задача реализовать программу, против которой можно сыграть в
"Быки и коровы"."""

correct_answer = "".join(map(str, random.sample(range(10), 4)))
print(f"Number to guess is: {correct_answer}")

while True:
    user_answer = input("Try to guess four-digit number (note: digits should "
                        "be unique): ")
    bulls = 0
    cows = 0
    for i in range(4):
        if correct_answer[i] == user_answer[i]:
            bulls += 1
        elif user_answer[i] in correct_answer:
            cows += 1
    print(f"{cows} cow(s), {bulls} bull(s)")
    if bulls == 4:
        print("You won!")
        break


"""2. Like:
Создайте программу, которая, принимая массив имён, возвращает строку
описывающая количество лайков (как в Facebook)."""

names_list = input("Enter comma-separated list of names: ")
names_list = names_list.split(", ")

if len(names_list) == 1 and "" not in names_list:
    print(f"{names_list[0]} likes this")
elif len(names_list) == 2:
    print(f"{names_list[0]} and {names_list[1]} like this")
elif len(names_list) == 3:
    print(f"{names_list[0]}, {names_list[1]} and {names_list[2]} like this")
elif len(names_list) > 3:
    print(f"{names_list[0]}, {names_list[1]} and {len(names_list) - 2}"
          f" others like this")
else:
    print("No one likes this")


"""3. BuzzFuzz:
Напишите программу, которая перебирает последовательность от 1 до 100.
Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа, а для
чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5 надо
печатать "FuzzBuzz". Иначе печатать число.
Вывод должен быть следующим:
1
2
fuzz
4
buzz
fuzz
7
8
.."""

list_of_numbers = [i for i in range(101)]
for i in list_of_numbers:
    if i % 3 == 0 and i % 5 == 0:
        print('BuzzFuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)


# Задания 4,5,7 в мерж реквесте с классной работой.

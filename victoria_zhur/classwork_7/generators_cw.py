# Генераторы:
"""1. Напишите функцию-генератор, которая вычисляет числа фибоначчи."""


def fibonacci(n: int):
    """
    This function calculates Fibonacci numbers
    """
    fib1, fib2 = 0, 1
    for i in range(n):
        fib1, fib2 = fib2, fib1 + fib2
        yield fib1


for fib in fibonacci(10):
    print(fib, end=" ")
print()


"""2. Напишите генератор списка который принимает список numbers = [34.6,
-203.4, 44.9, 68.3, -12.2, 44.6, 12.7] и возвращает новый список только с
положительными числами"""

numbers_list = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]


def positive_numbers(numbers_list: list) -> list:
    """
    This function returns only positive numbers
    """
    for el in numbers_list:
        if el >= 0:
            yield el


for el in positive_numbers(numbers_list):
    print(el, end=" ")
print()


"""3. Необходимо составить список чисел которые указывают на длину слов в
строке: sentence = "the quick brown fox jumps over the lazy dog", но только
если слово не "the"."""

sentence = "the quick brown fox jumps over the lazy dog"


def word_length(sentence: str) -> list:
    """
    This function returns length of words that are not equal to "the"
    """
    for word in sentence.split(" "):
        if word != "the":
            yield len(word)


for word in word_length(sentence):
    print(word, end=" ")
print()


"""1. Петя перешел в другую школу. На уроке физкультуры ему понадобилось
определить своё место в строю. Помогите ему это сделать. Программа получает на
вход не возрастающую последовательность натуральных чисел, означающих рост
каждого человека в строю. После этого вводится число X – рост Пети.
Выведите номер, под которым Петя должен встать в строй. Если в строю есть люди
с одинаковым ростом,таким же, как у Пети, то он должен встать после них.
rank([165, 163, 162, 160, 157, 157, 155, 154], 162)  #=> 3"""

group_height = [165, 163, 162, 160, 157, 157, 155, 154]
petya_height = int(input("Please, enter height of Petya: "))
place = 0

for i in group_height:
    if petya_height <= i:
        place += 1
print(place + 1)


"""2. Дан список чисел. Выведите все элементы списка, которые больше
предыдущего элемента.
[1, 5, 2, 4, 3]  #=> [5, 4]
[1, 2, 3, 4, 5] #=> [2, 3, 4, 5]"""

list_of_numbers = [1, 2, 3, 4, 5]
output_list = [list_of_numbers[i] for i in range(len(list_of_numbers)) if
               (list_of_numbers[i] > list_of_numbers[i - 1])]
print(output_list)


"""3. Напишите программу, принимающую зубчатый массив любого типа и
возвращающего его "плоскую" копию."""

jagged_list = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]
flat_list = []


def flat_copy(jagged_list: list) -> list:
    """
    This function transforms jagged list into flat list
    """
    for elm in jagged_list:
        if isinstance(elm, list):
            flat_copy(elm)
        else:
            flat_list.append(elm)
    return flat_list


print(flat_copy(jagged_list))


"""4. Напишите функцию, которая принимает на вход одномерный массив и два
числа - размеры выходной матрицы. На выход программа должна подавать матрицу
нужного размера, сконструированную из элементов массива."""


def reshape(inp_list: list, row: int, col: int):
    """This function reshapes  array into matrix of set size"""
    inp_list_copy = inp_list[:]
    reshape_list = []
    for i in range(row):
        reshape_list.append(inp_list_copy[:col])
        del inp_list_copy[:col]
    return reshape_list


print(reshape([1, 2, 3, 4, 5, 6], 2, 3))

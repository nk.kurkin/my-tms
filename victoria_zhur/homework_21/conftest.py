import pytest
from selenium import webdriver

URL = "http://the-internet.herokuapp.com"


@pytest.fixture(scope="module")
def driver():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


@pytest.fixture()
def get_main_page(driver):
    return driver.get(URL)

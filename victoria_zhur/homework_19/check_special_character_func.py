"""
проверяет, является ли переданный аргумент спец символом
"""

import string


def check_special_character(arg):
    return arg in string.punctuation

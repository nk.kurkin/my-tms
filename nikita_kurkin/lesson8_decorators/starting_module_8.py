from homework import simple_function, some_random_function, what_the_sum, \
    degree, some_string, some_string1, some_mathematics_example,\
    new_list_compreh, fib, add_two_symbols, add_three_symbols

if __name__ == '__main__':
    counter = 0
    while counter < 3:
        counter += 1
        what_user_want = input("Enter the number that you want: \n"
                               "1) Функция принимает 2 аргумента. "
                               "Декоратор, который печатает имя функции"
                               "и всего его аргументы \n"
                               "2) Декоратор с before-after \n"
                               "3) Декоратор +1\n"
                               "4) Декоратор - верхний регистр\n"
                               "5) Декоратор - имя функции\n"
                               "6) Декоратор - смена порядка неименованных"
                               "аргументов\n"
                               "7) Декоратор - таймер\n"
                               "8) Декоратор - таймер+имя функции\n"
                               "9) Декоратор - сложное выражение с"
                               "декораторами из п1 и п2\n"
                               "10) Декоратор - конвертация в стрингу"
                               "и конкатенация\n"
                               "11) Декоратор - конвертация в число"
                               "и сложение\n->")

        function_params_mapper = {
            "1": {"function": simple_function, "params": (5, 6)},
            "2": {"function": what_the_sum, "params": (5, 9)},
            "3": {"function": degree, "params": (2, 2)},
            "4": {"function": some_string, "params": 'hello lalala'},
            "5": {"function": some_string1, "params": ""},
            "6": {"function": some_random_function, "params": (1, 2, 3)},
            "7": {"function": new_list_compreh, "params": 100},
            "8": {"function": fib, "params": 1000},
            "9": {"function": some_mathematics_example, "params": (3, 30)},
            "10": {"function": add_two_symbols, "params": [("3", 5), (5, 5),
                                                           ('a', 'b')]},
            "11": {"function": add_three_symbols, "params": [(5, 6, 7),
                                                             ("3", 5, 0),
                                                             (0.1, 0.2, 0.4)]},
        }
        for k, v in function_params_mapper.items():
            if what_user_want == k:
                if isinstance(function_params_mapper.get(k)["params"], list):
                    for i in function_params_mapper.get(k)["params"]:
                        function_params_mapper.get(k)["function"](*i)
                elif isinstance(function_params_mapper.get(k)["params"], str)\
                        or isinstance(function_params_mapper.get(k)["params"],
                                      int):
                    function_params_mapper.get(k)["function"](
                        function_params_mapper.get(k)["params"])
                elif isinstance(function_params_mapper.get(k)["params"],
                                tuple):
                    function_params_mapper.get(k)["function"](
                        *function_params_mapper.get(k)["params"])

"""
Библиотека
Создайте класс book с именем книги, автором, кол-м страниц,
ISBN, флагом, зарезервирована ли книги или нет. Создайте класс
пользователь который может брать книгу, возвращать, бронировать.
Если другой пользователь хочет взять зарезервированную книгу
(или которую уже кто-то читает - надо ему про это сказать).
"""


class Book:
    def __init__(self, name, author, count_of_pages, isbn,
                 reserved=False, available=True):
        self.name = name
        self.author = author
        self.count_of_pages = count_of_pages
        self.isbn = isbn
        self.reserved = reserved
        self.available = available


class User:
    def __init__(self, name):
        """
        :param name: users' name
        has_book - does user has a book?
        what_the_book - whats the name of book does user have?
        booking_bookname - whats the name of book does user book?
        """
        self.name = name
        self.has_book = None
        self.what_the_book = None
        self.booking_bookname = None

    def get_book(self, book):
        if book.reserved and book.available and\
                self.booking_bookname == book.name:
            book.reserved = False
            book.available = False
            self.booking_bookname = None
            self.has_book = True
            self.what_the_book = book.name
            return 'Success! You can read it!'
        else:
            return 'Something wrong. Try to choose another' \
                   ' book, please!'

    def return_book(self, book):
        if self.has_book and not book.available and\
                self.what_the_book == book.name:
            book.available = True
            self.has_book = False
            self.what_the_book = None
            return 'Thanks for return this book! Good luck!'
        else:
            return 'You haven\'t a book for returning'

    def booking_book(self, book):
        if book.reserved or not book.available:
            return 'I\'m sorry. This book has already booked.' \
                   ' Choose another book!'
        else:
            book.reserved = True
            self.booking_bookname = book.name
            return 'Congrats! When you want to read it - get it!'


Harry_Potter = Book('Harry Potter and philosophy stone',
                    'J.K.Rowling', 320, '978-3-16-148410-0')
Pushkins_tails = Book('Pushkin\'s fairy tails', 'A.S.Pushkin',
                      153, '187-6-19-142340-1', reserved=True)
The_memoirs_of_Sherlock = Book('The Memoirs of Sherlock Holmes',
                               'Arthur Conan Doyle', 384,
                               '324-5-63-346146-5', available=False)
Coraline = Book('Coraline', 'Neil Gaiman', 319, '784-9-05-049576-51')


Alex = User('Alex')
John = User('John')
Tom = User('Tom')

print(Alex.booking_book(Harry_Potter))
print(Alex.get_book(Harry_Potter))
print(Alex.return_book(Harry_Potter))
print(John.booking_book(Harry_Potter))
print(Alex.booking_book(Pushkins_tails))
print(John.booking_book(The_memoirs_of_Sherlock))
print(John.get_book(Harry_Potter))
print(Tom.get_book(Pushkins_tails))
print(Tom.get_book(The_memoirs_of_Sherlock))
print(Tom.return_book(Pushkins_tails))

"""
Банковский вклад
Создайте класс инвестиция. Который содержит необходимые поля и
методы, например сумма инвестиция и его срок. Пользователь делает
инвестиция в размере N рублей сроком на R лет под 10% годовых
(инвестиция с возможностью ежемесячной капитализации - это означает,
что проценты прибавляются к сумме инвестиции ежемесячно).
Написать класс Bank, метод deposit принимает аргументы N и R, и
возвращает сумму, которая будет на счету пользователя.
https://myfin.by/wiki/term/kapitalizaciya-procentov
"""


class Investment:
    percent = 0.1

    def __init__(self, money: int, time: int, additional_money: int):
        """
        :param money: Base count of money which used for investment
        :param time: Count of years which investment will be active
        :param additional_money: Count of money which will
         be add every year
        """
        self.money = money
        self.time = time
        self.additional_money = additional_money

    def how_much_money_earn(self):
        sum_ = self.money
        for i in range(self.time):
            sum_ += self.money * Investment.percent + self.additional_money
        return sum_


Alex = Investment(10000, 5, 1000)
print(Alex.how_much_money_earn())


class Bank(Investment):
    def __init__(self, money, time, additional_money):
        """
        :param money: Base count of money which used for investment
        :param time: Count of years which investment will be active
        :param additional_money: Count of money which will
         be add every year
        """
        super().__init__(money, time, additional_money)

    def deposit(self):
        """
        :return: result - count of money which user could earn
        """
        result = self.money
        for i in range(self.time):
            result += self.money * Investment.percent + self.additional_money
        return result


Kate = Bank(1000, 5, 0)
print(Kate.deposit())

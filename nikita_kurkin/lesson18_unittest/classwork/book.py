
class PageNumberError(Exception):
    message = 'Wrong count of pages'

    def __init__(self):
        super().__init__(self.message)


class YearError(Exception):
    message = 'Wrong year'

    def __init__(self):
        super().__init__(self.message)


class AuthorError(Exception):
    message = 'Author should be only letters'

    def __init__(self):
        super().__init__(self.message)


class PriceError(Exception):
    message = 'Wrong price'

    def __init__(self):
        super().__init__(self.message)


class Book:
    def __init__(self, count_of_pages, year, author, price):
        self.count_of_pages = self._valid_count_pages(count_of_pages)
        self.year = self._valid_year(year)
        self.author = self._valid_author(author)
        self.price = self._valid_price(price)

    @staticmethod
    def valid_count_pages(count_of_pages):
        if count_of_pages < 4000:
            raise PageNumberError
        return count_of_pages

    @staticmethod
    def valid_year(year):
        if year > 1980:
            raise YearError
        return year

    @staticmethod
    def valid_author(author):
        if not author.isalpha():
            raise AuthorError
        return author

    @staticmethod
    def valid_price(price):
        if not 100 < price < 10000:
            raise PriceError
        return price

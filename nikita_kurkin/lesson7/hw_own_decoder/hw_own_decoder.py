"""
Шифр Цезаря — один из древнейших шифров. При шифровании каждый
символ заменяется другим, отстоящим от него в алфавите на
фиксированное число позиций

Примеры:
hello world! -> khoor zruog!
this is a test string -> ymnx nx f yjxy xywnsl

Напишите две функции - encode и decode принимающие как
параметр строку и число - сдвиг.
"""
import string


def encode_or_decode(encode_string: str, offset: int) -> str:
    """
    :param encode_string: String which will be encoding or decoding
    :param offset: offset in alphabet for encoding or decoding.
    If offset > 0 - encode, else decode
    :return: result of encode or decode
    """

    alphabet = string.printable[10:62]
    shifted_alphabet = alphabet[offset:] + alphabet[:offset]
    table = str.maketrans(alphabet, shifted_alphabet)
    return encode_string.translate(table)


if __name__ == "__main__":
    print(encode_or_decode('Hello World!', 3))
    print(encode_or_decode('this is a test string', 5))
    print(encode_or_decode('khoor zruog!', -3))
    print(encode_or_decode('ymnx nx f yjxy xywnsl', -5))
    print(encode_or_decode('ZzZZ', 4))

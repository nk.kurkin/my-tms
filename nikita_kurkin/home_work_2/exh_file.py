import statistics
import math

# first task
"""
Даны 2 действительных числа a и b. Получить их сумму, разность и
произведение."""

a = 5
b = 4
c = a + b
print(c)
c = a - b
print(c)
c = a * b
print(c)


# second task
"""
Даны действительные числа x и y. Получить |x-y| / 1+ |xy|
"""

x = 6
y = 3
result = (abs(6) - abs(3)) / (1 + abs(x * y))
print(result)


# third task
"""
Дана длина ребра куба. Найти объем куба и площадь его боковой
поверхности.
"""

a = 5
volume_cube = a ** 3
print(volume_cube)
square_cube = a ** 2
print(square_cube)


# fourth task
"""
Даны два действительных числа. Найти среднее арифметическое и
среднее геометрическое этих чисел
"""

digits = 8, 6
average_ar = statistics.mean(digits)
average_geom = statistics.geometric_mean(digits)
print(average_ar, ', ', average_geom, sep='')


# fifth task
"""
Даны катеты прямоугольного треугольника. Найти его гипотенузу и
площадь.
"""

a = 3
b = 4
c = math.sqrt(a ** 2 + b ** 2)
sqaure_rectangle = 0.5 * a * b
print(c)
print(sqaure_rectangle)

from selenium.webdriver.common.by import By


class WelcomePageLocators:
    LOGIN_INPUT = (By.ID, "user-name")
    PASSWORD_INPUT = (By.ID, "password")
    LOGIN_BUTTON = (By.ID, "login-button")


class CatalogPageLocators:
    MENU_BUTTON = (By.ID, "react-burger-menu-btn")
    FILTER_SELECT = (By.CLASS_NAME, "product_sort_container")
    TWITTER_LINK = (By.XPATH, "//li[@class='social_twitter']/a")
    FACEBOOK_LINK = (By.XPATH, "//li[@class='social_facebook']/a")
    LINKEDIN_LINK = (By.XPATH, "//li[@class='social_linkedin']/a")
    BIN_BUTTON = (By.CSS_SELECTOR, ".shopping_cart_container")

    PAGE_ITEMS = (By.CSS_SELECTOR, '.inventory_item')
    ITEM_PICTURE = (By.CLASS_NAME, 'inventory_item_img')
    ITEM_PRICE = (By.CLASS_NAME, 'inventory_item_price')
    ITEM_DESCRIPTION = (By.CLASS_NAME, 'inventory_item_desc')

    #
    # GOOD_1_PICTURE = (By.CSS_SELECTOR, "#item_4_img_link img")
    # GOOD_1_DESCRIPTION = (By.CSS_SELECTOR, "#item_4_title_link + div")
    # GOOD_1_PRICE = (By.CSS_SELECTOR,
    #                 '#inventory_container > div > div:nth-child(1) >'
    #                 ' div.inventory_item_description > div.pricebar > div')
    #
    # GOOD_2_PICTURE = (By.CSS_SELECTOR, "#item_0_img_link img")
    # GOOD_2_DESCRIPTION = (By.CSS_SELECTOR, "#item_0_title_link + div")
    # GOOD_2_PRICE = (By.CSS_SELECTOR,
    #                 '#inventory_container > div > div:nth-child(2) >'
    #                 ' div.inventory_item_description > div.pricebar > div')
    #
    # GOOD_3_PICTURE = (By.CSS_SELECTOR, "#item_1_img_link img")
    # GOOD_3_DESCRIPTION = (By.CSS_SELECTOR, "#item_1_title_link + div")
    # GOOD_3_PRICE = (By.CSS_SELECTOR,
    #                 '#inventory_container > div > div:nth-child(3) >'
    #                 ' div.inventory_item_description > div.pricebar > div')
    #
    # GOOD_4_PICTURE = (By.CSS_SELECTOR, "#item_5_img_link img")
    # GOOD_4_DESCRIPTION = (By.CSS_SELECTOR, "#item_5_title_link + div")
    # GOOD_4_PRICE = (By.CSS_SELECTOR,
    #                 '#inventory_container > div > div:nth-child(4) >'
    #                 ' div.inventory_item_description > div.pricebar > div')
    #
    # GOOD_5_PICTURE = (By.CSS_SELECTOR, "#item_2_img_link img")
    # GOOD_5_DESCRIPTION = (By.CSS_SELECTOR, "#item_2_title_link + div")
    # GOOD_5_PRICE = (By.CSS_SELECTOR,
    #                 '#inventory_container > div > div:nth-child(5) >'
    #                 ' div.inventory_item_description > div.pricebar > div')
    #
    # GOOD_6_PICTURE = (By.CSS_SELECTOR, "#item_3_img_link img")
    # GOOD_6_DESCRIPTION = (By.CSS_SELECTOR, "#item_3_title_link + div")
    # GOOD_6_PRICE = (By.CSS_SELECTOR,
    #                 '#inventory_container > div > div:nth-child(6) >'
    #                 ' div.inventory_item_description > div.pricebar > div')

# Общий модуль
# Создать модуль, который будет предоставлять возможность выбрать одну из
# двух предыдущих программ и запустить ее, также он должен позволить по
# окончанию выполнения программы выбрать запуск другой!
# Если пользователь совершит ошибку при выборе программы
# Первые три раза программа должна сообщить ему об ошибке, а на четвертый раз
# запустить одну из программ, выбранных случайно.

import random


def module_call(x: int):
    """
    The function imports the module that user chooses.
    """
    if x == 1:
        from bank_task import bank_task
        a, years = bank_task.bank_input_check()
        print('Your final sum: ', bank_task.bank(a, years))
    else:
        from Caesar_task import Caesar_task
        input_string, shift = Caesar_task.caesar_input_check()
        print('The final string: ',
              Caesar_task.encode_decode(input_string, shift))


def choice():
    """
    This function allows to choose what module do you want to launch:
    bank_task or Caesar_task. The function has wrong data input defence.
    """
    k = 0
    while k != 3:
        try:
            print('Choose the module to run: ', '1) Bank_task',
                  '2) Caesar_task', sep='\n')
            user_choice = int(input())
            if user_choice not in [1, 2]:
                print('Wrong data input. Try again')
                k += 1
                continue
            else:
                break
        except ValueError:
            k += 1
            if k != 3:
                print('Wrong data input. Try again')
    if k == 3:
        print('The random module is launching...')
        user_choice = random.randint(1, 2)
    module_call(user_choice)

    while True:
        try:
            print('Do you want to launch another module? ', '1) Yes',
                  '2) No', sep='\n')
            user_choice_end = int(input())
            if user_choice_end not in [1, 2]:
                print('Wrong data input. Try again')
                continue
            else:
                break
        except ValueError:
            print('Wrong data input. Try again')
    if user_choice_end == 2:
        print('Thanks and good luck')
    else:
        if user_choice == 1:
            user_choice = 2
        else:
            user_choice = 1
        module_call(user_choice)


if __name__ == '__main__':
    choice()

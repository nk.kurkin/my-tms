# создайте класс работник
# У класса есть конструктор, в который надо передать имя и ЗП.
# У класса есть 2 метода: получение имени сотрудника и получение количества
# созданных работников
# При создании экземпляра класса должен вызываться метод, который отвечает за
# увеличение количества работников на 1.

class Worker:
    count = 0

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Worker.count = +1

    @classmethod
    def info(cls):
        return Worker.count

    def info_n(self):
        return self.name


worker1 = Worker('Vasya', 100)
print(Worker.info())
print(worker1.info_n())


# Путешествие
# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Класс студен описан следующим образом:
# class Student:
# def __init__(self, name, money):
# self.name = name
# self.money = money
# Необходимо понять у кого больше всего денег и вернуть его имя.
# Если у студентов денег поровну вернуть: “all”.
# (P.S. метод подсчета не должен быть частью класса)

class Student:
    a = {}

    def __init__(self, name, money):
        self.name = name
        self.money = money
        Student.a.setdefault(self.name, self.money)

    @staticmethod
    def max_money():
        """
        This method finds the person with more money than others.
        """
        b = []
        money_list = list(Student.a.values())
        if money_list.count(max(Student.a.values())) == len(money_list):
            return 'all'
        for i in Student.a.keys():
            if Student.a[i] == max(Student.a.values()):
                b.append(i)
        return b

    def max_money_2(arg: list):
        """
        This function takes a list of instances as argument and returns the
        instance parameter 'name' with highest parameter 'money'.
        """
        st_money = [student.money for student in arg]
        max_money = max(st_money)
        if st_money.count(max_money) == len(st_money):
            return 'all'
        for i in arg:
            if i.money == max_money:
                return i.name


s1 = Student('vasya', 1000)
s2 = Student('masha', 9000)
print(Student.a)
print(Student.max_money())
print(Student.max_money_2([s1, s2]))
assert Student.max_money_2([s1, s2]) == 'masha'

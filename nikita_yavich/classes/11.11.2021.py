# First task:types

# Переменной var_int присвойте значение 10, var_float - значение 8.4,
# var_str - "No"

var_int = 10
var_float = 8.4
var_str = 'No'

# Измените значение, хранимое в переменной var_int, увеличив его в 3.5
# раза, результат свяжите с переменной big_int.

big_int = var_int * 3.5

# Измените значение, хранимое в переменной var_float, уменьшив его на единицу,
# результат свяжите с той же переменной.

var_float -= 1

# Разделите var_int на var_float, а затем big_int на var_float. Результат
# данных выражений не привязывайте ни к каким переменным.

print(var_int / var_float)
print(big_int / var_float)

# Измените значение переменной var_str на "NoNoYesYesYes".
# При формировании нового значения используйте операции конкатенации (+)
# и повторения строки (*).

var_str = var_str * 2 + 'Yes' * 3

# Выведите значения всех переменных.

print(var_int)
print(var_float)
print(var_str)
print(big_int)

# second_task:

# Свяжите переменную с любой строкой, состоящей не менее чем из 8 символов.
# Извлеките из строки первый символ, затем последний, третий с начала и
# третий с конца. Измерьте длину вашей строки.

a = 'touch_the_sky'
print(a[0])
print(a[-1])
print(a[2])
print(a[-3])
print(len(a))

# Присвойте произвольную строку длиной 10-15 символов переменной и
# извлеките из нее следующие срезы:
# - первые восемь символов
# - четыре символа из центра строки
# - символы с индексами кратными трем
# - переверните строку

b = 'feelthepower'
print(b[0:9])
string_lenght = int(len(b) / 2)
print(b[string_lenght:10])
print(b[3::3])
print(b[::-1])

# Есть строка: “my name is name”.
# Напечатайте ее, но вместо 2ого “name” вставьте ваше имя.

c = 'my name is name'
print(c[0:11] + 'NIKITA')

# Есть строка: test_tring = "Hello world!", необходимо напечатать на каком
# месте находится буква w, кол-во букв l. Проверить начинается ли строка с
# подстроки: “Hello”. Проверить заканчивается ли строка
# подстрокой: “qwe”

test_string = 'Hello world!'
print(test_string.index('w'))
print(test_string.count('l'))
print('Hello' == test_string[0:5])
print('qwe' == test_string[-1:-4])
# либо:
print(test_string.startswith('Hello'))
print(test_string.endswith('qwe'))

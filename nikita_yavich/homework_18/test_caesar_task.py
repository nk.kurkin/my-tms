from Caesar_task.Caesar_task import encode_decode
import unittest


class TestCaesarTaskOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for OK tests...")

    @unittest.skip("Другие тесты покрывают кейс")
    def test_encode_lower_letters(self):
        self.assertEqual(encode_decode('abc', 1), 'bcd')

    @unittest.skip("Другие тесты покрывают кейс")
    def test_encode_upper_letters(self):
        self.assertEqual(encode_decode('ABC', 1), 'BCD')

    @unittest.skip("Другие тесты покрывают кейс")
    def test_encode_upper_n_lower_letters(self):
        self.assertEqual(encode_decode('AbC', 1), 'BcD')

    @unittest.skip("Другие тесты покрывают кейс")
    def test_decode_text(self):
        self.assertEqual(encode_decode('bCd', -1), 'aBc')

    def test_encode_wrong_output(self):
        self.assertNotEqual(encode_decode('xyz', 1), 'yZa')

    def test_decode_wrong_output(self):
        self.assertNotEqual(encode_decode('abc', -1), 'zAb')

    def test_encode_end_limit(self):
        self.assertEqual(encode_decode('xYz', 1), 'yZa')

    def test_decode_start_limit(self):
        self.assertEqual(encode_decode('aBc', -1), 'zAb')

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearsDownClass. Finishing OK tests...")


@unittest.expectedFailure
class TestCaesarTaskFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for Fail tests...")

    def test_encode_end_limit(self):
        self.assertEqual(encode_decode('xYz', 1), 'yza')

    def test_decode_start_limit(self):
        self.assertEqual(encode_decode('aBc', -1), 'zab')

    def test_encode_wrong_output(self):
        self.assertNotEqual(encode_decode('xYz', 1), 'yZa')

    def test_decode_wrong_output(self):
        self.assertNotEqual(encode_decode('aBc', -1), 'zAb')

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearsDownClass. Finishing Fail tests...")

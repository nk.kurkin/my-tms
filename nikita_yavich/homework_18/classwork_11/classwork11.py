# Создать класс Book. Атрибуты: количество страниц, год издания, автор,
# цена. Добавить валидацию в конструкторе на ввод корректных данных


class PagesException(Exception):
    def __init__(self, message='Sheets amount must be less then 4000'):
        super().__init__(message)


class YearException(Exception):
    def __init__(self, message='Year must be after 1980'):
        super().__init__(message)


class AuthorException(Exception):
    def __init__(self, message='The authors name must include only letters'):
        super().__init__(message)


class PriceException(Exception):
    def __init__(self, message='Price should be between 100 and 10000 '):
        super().__init__(message)


class Book:
    def __init__(self, pages, year, author, price):
        self.pages = self.validate_pages(pages)
        self.year = self.validate_year(year)
        self.author = self.validate_author(author)
        self.price = self.validate_price(price)

    @staticmethod
    def validate_pages(pages):
        if pages < 4000:
            return pages
        raise PagesException

    @staticmethod
    def validate_year(year):
        if year > 1980:
            return year
        raise YearException

    @staticmethod
    def validate_author(author):
        if author.isalpha():
            return author
        raise AuthorException

    @staticmethod
    def validate_price(price):
        if price > 100 and price < 10000:
            return price
        raise PriceException

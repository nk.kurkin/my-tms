import calculator
import unittest


class TestCalculatorFindSumFuncOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for OK tests...")

    def test_find_sum_positive_numbers(self):
        self.assertEqual(calculator.find_sum(2, 2), 4)

    def test_find_sum_negative_numbers(self):
        self.assertEqual(calculator.find_sum(-3, -3), -6)

    def test_find_sum_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_sum(-3, 2), -1)

    def test_find_sum_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_sum(2, 2), 5)

    def test_find_sum_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_sum(-3, -3), 6)

    def test_find_sum_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_sum(-3, 2), 1)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing OK tests...")


@unittest.expectedFailure
class TestCalculatorFindSumFuncFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for Fail tests...")

    def test_find_sum_positive_numbers(self):
        self.assertEqual(calculator.find_sum(2, 2), 3)

    def test_find_sum_negative_numbers(self):
        self.assertEqual(calculator.find_sum(-3, -3), 6)

    def test_find_sum_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_sum(-3, 2), 5)

    def test_find_sum_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_sum(2, 2), 4)

    def test_find_sum_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_sum(-3, -3), -6)

    def test_find_sum_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_sum(-3, 2), -1)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing Fail tests...")


class TestCalculatorFindDiffFuncOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for OK tests...")

    def test_find_diff_positive_numbers(self):
        self.assertEqual(calculator.find_diff(2, 2), 0)

    def test_find_diff_negative_numbers(self):
        self.assertEqual(calculator.find_diff(-3, -3), 0)

    def test_find_diff_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_diff(-3, 2), -5)

    def test_find_diff_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_diff(2, 2), 4)

    def test_find_diff_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_diff(-3, -3), -6)

    def test_find_diff_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_diff(-3, 2), 1)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing OK tests...")


@unittest.expectedFailure
class TestCalculatorFindDiffFuncFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for Fail tests...")

    def test_find_diff_positive_numbers(self):
        self.assertEqual(calculator.find_diff(2, 2), 4)

    def test_find_diff_negative_numbers(self):
        self.assertEqual(calculator.find_diff(-3, -3), -6)

    def test_find_diff_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_diff(-3, 2), 5)

    def test_find_diff_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_diff(2, 2), 0)

    def test_find_diff_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_diff(-3, -3), 0)

    def test_find_diff_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_diff(-3, 2), -5)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing Fail tests...")


class TestCalculatorFindMultipleFuncOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for OK tests...")

    def test_find_multiple_positive_numbers(self):
        self.assertEqual(calculator.find_multiple(2, 2), 4)

    def test_find_multiple_negative_numbers(self):
        self.assertEqual(calculator.find_multiple(-3, -3), 9)

    def test_find_multiple_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_multiple(-3, 2), -6)

    def test_find_multiple_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_multiple(2, 2), -4)

    def test_find_multiple_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_multiple(-3, -3), -9)

    def test_find_multiple_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_multiple(-3, 2), 6)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing OK tests...")


@unittest.expectedFailure
class TestCalculatorFindMultipleFuncFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for Fail tests...")

    def test_find_multiple_positive_numbers(self):
        self.assertEqual(calculator.find_multiple(2, 2), -4)

    def test_find_multiple_negative_numbers(self):
        self.assertEqual(calculator.find_multiple(-3, -3), -9)

    def test_find_multiple_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_multiple(-3, 2), 6)

    def test_find_multiple_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_multiple(2, 2), 4)

    def test_find_multiple_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_multiple(-3, -3), 9)

    def test_find_multiple_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_multiple(-3, 2), -6)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing Fail tests...")


class TestCalculatorFindDivFuncOK(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for OK tests...")

    def test_find_div_positive_numbers(self):
        self.assertEqual(calculator.find_div(2, 2), 1)

    def test_find_div_negative_numbers(self):
        self.assertEqual(calculator.find_div(-3, -3), 1)

    def test_find_div_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_div(-3, 3), -1)

    def test_find_div_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_div(2, 2), 2)

    def test_find_div_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_div(-3, -3), -1)

    def test_find_div_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_div(-3, 3), 1)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing OK tests...")


@unittest.expectedFailure
class TestCalculatorFindDivFuncFail(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("FIXTURE setUpClass. Preparing for Fail tests...")

    def test_find_div_positive_numbers(self):
        self.assertEqual(calculator.find_div(2, 2), -1)

    def test_find_div_negative_numbers(self):
        self.assertEqual(calculator.find_div(-3, -3), -1)

    def test_find_div_positive_and_negative_numbers(self):
        self.assertEqual(calculator.find_div(-3, 3), 1)

    def test_find_div_positive_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_div(2, 2), 1)

    def test_find_div_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_div(-3, -3), 1)

    def test_find_div_positive_and_negative_numbers_wrong_output(self):
        self.assertNotEqual(calculator.find_div(-3, 3), -1)

    @classmethod
    def tearDownClass(cls):
        print("FIXTURE tearDownClass. Finishing Fail tests...")

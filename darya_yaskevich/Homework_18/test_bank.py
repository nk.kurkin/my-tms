from bank import bank
from unittest import TestCase


class TestBank(TestCase):

    def test_bank_positive(self):
        self.assertEqual(bank(1000, 1), 1100)
        self.assertEqual(bank(1000, 2), 1210)

    def test_bank_empty_string_as_amount(self):
        with self.assertRaises(TypeError):
            bank('', 2)

    def test_bank_text_as_amount(self):
        with self.assertRaises(TypeError):
            bank('a', 2)

    def test_bank_float_amount(self):
        self.assertEqual(bank(1000.25, 2), 1210.3025)

    def test_bank_zero_amount(self):
        self.assertEqual(bank(0, 2), 0)

    def test_bank_empty_string_as_years(self):
        with self.assertRaises(TypeError):
            bank(500, '')

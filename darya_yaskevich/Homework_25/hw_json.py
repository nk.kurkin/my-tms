import json

json_file = 'C:\\data\\HomeWork_25\\students.json'

with open(json_file, "r") as f:
    students = json.load(f)


def get_students_by_class(class_name: str):
    return list(
        filter(lambda student: student["Class"].lower() == class_name.lower(),
               students))


def get_students_by_club(club_name: str):
    return list(
        filter(lambda student: student["Club"].lower() == club_name.lower(),
               students))


def get_students_by_gender(gender: str):
    return list(
        filter(lambda student: student["Gender"].lower() == gender.lower(),
               students))


def get_students_by_name(name: str):
    return list(
        filter(lambda student: name.lower() in student["Name"].lower(),
               students))


if __name__ == "__main__":
    search = "Senp"
    print(get_students_by_name(search))

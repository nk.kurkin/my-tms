import yaml
import json

file_yaml = 'C:\\data\\HomeWork_25\\order.yaml'

with open(file_yaml, "r") as f:
    order = yaml.safe_load(f)

# напечатать номер заказа
print("invoice:", order["invoice"])

# напечатать адрес отправки
for key, value in order['bill-to']['address'].items():
    print(key, ":", value)

# напечатать описание посылки, стоимость и кол-во
print("order details:")
print('quantity:', len(order['product']))

for item in order['product']:
    print(item)

print('total:', order['total'])

# конвертировать yaml файл в json
with open('order_in_json.json', "w") as json_out:
    json.dump(order, json_out, default=str, indent=4)

# создать свой yaml файл
with open('new.yaml', "w") as d_output:
    yaml.dump(order, d_output)

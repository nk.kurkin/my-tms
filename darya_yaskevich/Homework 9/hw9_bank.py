class Investment:
    def __init__(self, n, r):
        self.principal = n
        self.total_years = r


inv1 = Investment(1000, 5)


class Bank:
    an_int = 0.1   # annual interest
    c = 12         # compounding periods per year

    @classmethod
    def deposit(cls, investment):
        final_amount = investment.principal * (
            pow((1 + Bank.an_int / Bank.c), Bank.c * investment.total_years)
        )
        currency = "${:,.2f}".format(final_amount)
        return currency


print(Bank.deposit(inv1))

# This module includes the function for calculating the bank deposit
# and the function to launch the program.
def bank(a: float, years: int):
    """This function calculates the bank deposit (a - the deposit amount).
    It returns the amount that will be on the user's account."""
    for i in range(years):
        a = a + (a * 0.1)
    return a


def run_deposit():
    """This function launches the program to calculate a deposit."""
    print('Calculate bank deposit')
    deposit = float(input('Deposit amount: '))
    period = int(input('Investment term (years): '))
    print('Calculated account balance:', bank(deposit, period))


if __name__ == "__main__":
    run_deposit()

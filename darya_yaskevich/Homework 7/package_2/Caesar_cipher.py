# This module includes the function to decrypt/encrypt English or Russian
# text with Caesar cipher and the main function to launch the program.
# The cipher preserves the original case and punctuation marks.
en_lower = 'abcdefghijklmnopqrstuvwxyz'
en_upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
ru_lower = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
ru_upper = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"


def caesar(text: str, k: int):
    """
    This function encrypts or decrypts the text with a right shift.
    :param text: the text to encrypt or decrypt
    :param k: the key (the shift parameter)
    :return: formatted text
    """
    length = None
    final, lower, upper = '', '', ''
    for char in text:
        if char in en_lower or char in en_upper:
            length = 26
            lower = en_lower
            upper = en_upper
        else:
            length = 33
            lower = ru_lower
            upper = ru_upper
        break
    for char in text:
        if char.isalpha():
            if char.islower():
                alphabet = lower
            else:
                alphabet = upper
            x = alphabet.find(char)
            y = (x + k) % length
            letter = alphabet[y]
            final += letter
        else:
            final += char
    return final


def run_cipher():
    """This function launches encryption/decryption program."""
    print('Decrypt / encrypt with Caesar')
    choice = input('Encrypt or decrypt? (e/d) ').lower()
    while choice != 'e' and choice != 'd':
        choice = input(
            "Wrong input. Enter 'e' or 'd' (encrypt/decrypt): ").lower()
    key = input('Specify the key (digit): ')
    while not key.isdigit():
        key = input("Wrong input. The key value must be numeric: ").lower()
    key = int(key)
    if choice == 'd':
        key = -key
    txt = input('Input text: ')
    print(caesar(txt, key))


if __name__ == "__main__":
    run_cipher()

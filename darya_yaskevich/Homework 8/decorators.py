import time
from functools import wraps


def wrap_with_words(func):
    """
    The decorator prints before and after the function call the words
    'Before' and 'After'.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Before')
        print(func(*args, **kwargs))
        print('After')
    return wrapper


def add_one(func):
    """This decorator function adds 1 to a given number."""
    @wraps(func)
    def inner(*args):
        print('Number:', func(*args))
        return f'Number after addition: {func(*args) + 1}'
    return inner


def uppercase(func):
    """This decorator function converts the input text to uppercase."""
    @wraps(func)
    def wrapper():
        return f'Formatted text: {func().upper()}'
    return wrapper


def func_name(func):
    """This decorator prints the name of the function."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        print(f'Function name: {func.__name__}')
        return func(*args, **kwargs)
    return wrapper


def change(func):
    """
    This decorator makes the function take its unnamed arguments
    in the reverse order.
    """
    @wraps(func)
    def inner(*args):
        return func(*args[::-1])
    return inner


def timer(func):
    """This decorator measures the function's execution time."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.perf_counter()
        func(*args, **kwargs)
        end = time.perf_counter()
        return 'Executed in:', end - start
    return wrapper


def typed(type_args='str'):
    """This decorator converts parameters, if necessary, and adds them up
    according to type(str/int/float).
    """
    def wrapper(func):
        @wraps(func)
        def inner(*args):
            if type_args == 'str':
                args = tuple([str(i) for i in args])
            else:
                if isinstance(args[0], float):
                    args = tuple([float(i) for i in args])
                else:
                    args = tuple([int(i) for i in args])
            return func(*args)
        return inner
    return wrapper

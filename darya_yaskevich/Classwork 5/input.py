# 1.
name = input('What is your name? ')
age = input('How old are you? ')
city = input('Where do ypu live? ')

print(f'This is {name}')
print(f'It is {age}')
print(f'(S)he lives in {city}')

# 2. Напишите программу, которая предлагала бы пользователю решить пример.
# Потом выводила бы на экран правильный ответ и ответ пользователя.
print('Solve this math problem: "4 * 100 - 54"')
answer = input('Answer: ')
result = str(4 * 100 - 54)

print(f'The right answer is {result}.')
print(f'Your answer is {answer}.')

# 3. Запросите у пользователя четыре числа.
# Отдельно сложите первые два и отдельно вторые два.
# Разделите первую сумму на вторую.
# Выведите результат так, чтобы ответ содержал две цифры после запятой.
a, b, c, d = [int(input()) for _ in range(4)]
first_two = a + b
second_two = c + d
result = first_two / second_two

print(round(result, 2))

# 1.
# Из заданной строки получить нужную строку:
# "String" => "SSttrriinngg"
# "Hello World" => "HHeelllloo  WWoorrlldd"
# "1234!_ " => "11223344!!__  "
s1 = input()
lst1 = []
for letter in s1:
    letter *= 2
    lst1.append(letter)
print(''.join(lst1))

# 2.
# Если х в квадрате больше 1000 - пишем "Hot" иначе - "Nope".
x = int(input())

if x ** 2 > 1000:
    print('Hot')
else:
    print('Nope')

# 3. (В домашней работе)
# 4. Подсчет букв
# Дано строка и буква => "fizbbbuz","b", нужно подсчитать сколько раз
# буква b встречается в заданной строке.
# "Hello there", "e" == 3
# "Hello there", "t" == 1
# "Hello there", "h" == 2
# "Hello there", "L" == 2
# "Hello there", " " == 1
s = input()
letter = input().lower()
count = s.count(letter)

print(count)

# 5. (В домашней работе)
# 6. Напишите программу, которая по данному числу n от 1 до n
# выводит на экран n флагов. Изображение одного флага
# имеет размер 4×4 символов. Внутри каждого флага должен быть записан
# его номер — число от 1 до n.
n = int(input())
count = 1

for i in range(1, n + 1):
    print(f'''+___
|{count} /
|__\\
|
''')
    count += 1

# 7. Есть 2 переменные salary и bonus. Salary - integer, bonus - boolean.
# Если bonus - true, salary должна быть умножена на 10. Если false - нет.
# 78, False == '$78'
salary = 60000
bonus = False

if bonus:
    salary *= 10

print(f'${salary}')

# 8. (В домашней работе)
# 9. Суммирование. Необходимо подсчитать сумму всех чисел до заданного:
# дано число 2, результат будет -> 3(1+2)
# дано число 8, результат будет -> 36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)
number = 22
total = 0

for i in range(1, number + 1):
    total += i

print(total)

# 10. (В домашней работе)

# IF
# 1. Если значение переменной больше 0, вывести специальное сообщение.

num = int(input())
if num > 0:
    print('Special message!')

# 2. Усовершенстовать код из задания 1 с помощью else, чтобы выводилась
# либо 1, либо -1.
num = int(input())
if num > 0:
    print(1)
else:
    print(-1)

# 3. Придумать программу с инструкцией if (желательно с веткой elif).
a = int(input('Enter the first number: '))
b = int(input('Enter the second number: '))

if a > b:
    print('The first number is greater.')
elif a < b:
    print('The second number is greater.')
else:
    print('Numbers are equal.')

# ELIF
# 1. Если значение первой переменной больше второй, то найти разницу
# значений переменных (вычесть из 1-й 2-ю), результат связать с третьей;
# если первая переменная имеет меньшее значение, чем вторая, то третью
# переменную связать с результатом суммы значений двух первых переменных;
# во всех остальных случаях, присвоить 3-ей переменной значение 1-й;
# вывести значение третьей переменной на экран.
a = int(input())
b = int(input())

if a > b:
    c = a - b
elif a < b:
    c = a + b
else:
    c = a

print(c)

# 2. Придумайте программу, в которой бы использовалась инструкция
# if-elif-else. Количество ветвей должно быть как минимум четыре.
summer = ['june', 'july', 'august']
autumn = ['september', 'october', 'november']
winter = ['december', 'january', 'february']
spring = ['march', 'april', 'may']
month = input('Enter the name of the month: ').lower()

if month in spring:
    print("It's spring!")
elif month in summer:
    print("It's summer!")
elif month in autumn:
    print("It's autumn!")
else:
    print("It's winter!")

# FOR
# 1. Создайте список, состоящий из четырех строк.
# Затем, с помощью цикла for, выведите строки поочередно на экран.
colors = ['red', 'green', 'purple', 'black']
for color in colors:
    print(color)

# 2. Измените предыдущую программу так,
# чтобы в конце каждой буквы строки добавлялось тире.
colors = ['grey', 'white', 'black']
colors_edited = []
for color in colors:
    for letter in color:
        letter += '-'
        colors_edited.append(letter)
for color in colors_edited:
    print(color)

# 3. Создайте список, содержащий элементы целочисленного типа,
# затем с помощью цикла перебора измените тип данных элементов
# на числа с плавающей точкой.
numbers = [1, 2, 3, 4, 5, 6]
numbers = [float(number) for number in numbers]

# 4. Есть два списка с одинаковым количеством элементов, в каждом из них
# есть элемент “hello”. В первом списке индекс этого элемента 1, во втором 7.
# Одинаковых элементов в обоих списках может быть несколько.
# Напишите программу которая распечатает индекс слова “hello”
# в первом списке и условный номер списка, индекс слова во втором списке
# и условный номер списка со словом Совпадают.
lst_of_lists = [
    ['goodbye', 'hello', 'bye-bye', '3', '4', '5', '6', '7'],
    ['goodbye', 'smthelse', '2', '3', '4', '5', '6', 'hello']
]
word_indices = []

for lst in lst_of_lists:
    ind = lst.index('hello')
    word_indices.append(ind)

print(f'Совпадают {word_indices[0]}-й элемент из первого списка'
      f' и {word_indices[1]}-й элемент из второго списка.')

# WHILE
# 1. Числа Фибоначчи (с 5-го по 20-й члены последовательности).
num1 = 1
num2 = 1
counter = 0
while counter < 20:
    if counter >= 5:
        print(num1)
    num1, num2 = num2, num1 + num2
    counter += 1
    if counter == 20:
        break

# 2. Напишите цикл, выводящий ряд четных чисел от 0 до 20.
number = 0
while number <= 20:
    print(number, end=' ')
    number += 2

# Цикл, выводящий каждое третье число в ряде от -1 до -21.
number = 0
while number > -21:
    number -= 3
    print(number, end=' ')

# 3. Самостоятельно придумайте программу на Python,
# в которой бы использовался цикл while.
# Нужно угадать число от 1 до 10. Если игрок не угадал,
# он получает подсказки (число больше или меньше), пока не угадает.
print("Guess the secret number!")
secret_num = 7

while True:
    number = input('Pick a number between 1 and 10: ')
    if number not in '12345678910':
        print('Oops! Enter a number from 1 to 10.')
        continue
    else:
        number = int(number)
        if number > secret_num:
            print('Too high!')
            continue
        elif number < secret_num:
            print('Too low!')
            continue
        else:
            print('You won!')
            break

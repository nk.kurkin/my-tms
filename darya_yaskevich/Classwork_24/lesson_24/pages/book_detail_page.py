from pages.base_page import BasePage
from pages.book_detail_page_locators import BookDetailPageLocators


class BookDetailPage(BasePage):
    def __init__(self, driver, url):
        super().__init__(driver, url)

    def get_image(self):
        return self.driver.find_element(*BookDetailPageLocators.BOOK_IMAGE)

    def get_book_name(self):
        return self.driver.find_element(*BookDetailPageLocators.BOOK_NAME).text

import pytest
from convert_to_float import convert_to_float, InputTypeError
from check_lowercase import lowercase
from spacial_char import special_chars


@pytest.mark.parametrize('test_input, expected', [
    ('Test', False),
    ('TEST', False),
    ('test', True)
])
def test_lowercase(test_input, expected):
    assert lowercase(test_input) == expected


@pytest.mark.parametrize('test_input', [1, 1.5, 0])
def test_convert_to_float(test_input):
    assert isinstance(convert_to_float(test_input), float)


@pytest.mark.parametrize('test_input, expected', [
    ('d', False),
    ('/', True),
    ('@', True)
])
def test_special(test_input, expected):
    assert special_chars(test_input) == expected


def test_convert_to_float_error():
    with pytest.raises(InputTypeError):
        convert_to_float('e')

from random import choice

print("Task 1; Быки и коровы")
# Ваша задача реализовать программу, против которой можно сыграть
# в "Быки и коровы"
a = '0123456789'
a1 = choice(a[1:10])

for i in range(3):
    a = ''.join(a.split(a1[i]))
    a1 += choice(a)
while True:
    z = input("Введите 4-значное число с неповторяющимися цифрами: ")
    b = 0
    c = 0
    for i in range(4):
        if a1[i] == z[i]:
            b += 1
        elif z[i] in a1:
            c += 1
    print('Число', z, 'содержит:', b, 'быка(-ов) и', c, 'коров(-ы)')
    if b == 4:
        print('Вы победили')
        break

print("Task 2; Like")
# Создайте программу, которая, принимая массив имён, возвращает строку
# описывающая количество лайков (как в Facebook)
names = input('Введите имена через запятую:').split(',')


print("Task 3; BuzzFuzz")
# Напишите программу, которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
# а для чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и
# 5 надо печатать "FuzzBuzz". Иначе печатать число

for number in range(0, 100):
    if number % 3 == 0:
        print('Fuzz')
    elif number % 5 == 0:
        print('Buzz')
    elif number % 3 == 0 and number % 5 == 0:
        print('FuzzBuzz')
    else:
        print(number)

print("Task 4")
# Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет “:” и пробел [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]

listik1 = ["a", "b", "c"]
listik2 = []
count = 1
for numerate in listik1:
    numerate = str(count) + ':' + numerate
    listik2.append(numerate)
    count += 1
print(listik2)

print("Task 5")
# Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True


def is_equal(listik_1: list):
    if not listik_1:
        return True
    else:
        return len(listik_1) == listik_1.count(listik_1[0])


listik_2 = []
print(is_equal(listik_2))

import unittest
from calculator import find_sum, find_diff, find_multiple, find_div


class TestSumCalculator(unittest.TestCase):

    def test_two_positive(self):
        self.assertEqual(find_sum(1, 1), 2)

    def test_zero_positive(self):
        self.assertEqual(find_sum(0, 1), 1)

    @unittest.expectedFailure
    def test_two_negative(self):
        self.assertEqual(find_sum(-1, -1), -3)


class TestDiffCalculator(unittest.TestCase):

    def test_two_positive(self):
        self.assertEqual(find_diff(1, 1), 0)

    def test_zero_positive(self):
        self.assertEqual(find_diff(0, 1), -1)

    @unittest.expectedFailure
    def test_two_negative(self):
        self.assertEqual(find_diff(-1, -1), 1)


class TestMultipleCalculator(unittest.TestCase):

    def test_two_positive(self):
        self.assertEqual(find_multiple(1, 1), 1)

    def test_zero_positive(self):
        self.assertEqual(find_multiple(0, 1), 0)

    @unittest.expectedFailure
    def test_two_negative(self):
        self.assertEqual(find_multiple(-1, -1), 3)


class TestDivCalculator(unittest.TestCase):

    def test_two_positive(self):
        self.assertEqual(find_div(1, 1), 1)

    def test_two_negative(self):
        self.assertEqual(find_div(-1, -1), 1)

    @unittest.expectedFailure
    def test_div_by_zero(self):
        self.assertEqual(find_div(1, 0), 1)


if __name__ == '__main__':
    unittest.main()

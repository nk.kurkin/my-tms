import random

# Hero
# В игре есть солдаты и герои. У каждого есть уникальный номер, и свойство,
# в котором хранится принадлежность команде. У солдат есть метод
# "move_to_hero",
# который в качестве аргумента принимает объект типа "Hero". У героев есть
# метод увеличения собственного уровня level_up.
# В основной ветке программы создается по одному герою для каждой команды.
# В цикле генерируются объекты-солдаты. Их принадлежность команде определяется
# случайно. Солдаты разных команд добавляются в разные списки.
# Измеряется длина списков солдат противоборствующих команд и выводится на
# экран. У героя, принадлежащего команде с более длинным списком, поднимается
# уровень. Отправьте одного из солдат первого героя следовать за ним.
# Выведите на экран идентификационные номера этих двух юнитов.


class Solider:
    id_ = 1

    def __init__(self):
        self.id_ = Solider.id_
        Solider.id_ += 1

    def move_to_hero(self, hero):
        print(f'The solider ID:{self.id_} follows hero ID:{hero.id_}')


class Hero(Solider):

    def __init__(self, team):
        Solider.__init__(self)
        self.team = team
        self.level = 1

    def level_up(self):
        self.level += 1
        print(f'The hero ID:{self.id_} up lvl {self.level}')


if __name__ == "__main__":
    hero_red = Hero('red')
    hero_blue = Hero('blue')
    team_red = []
    team_blue = []
    team_red.append(Solider())

    team_mapper = {
        "red": team_red,
        "blue": team_blue
    }

    for i in range(20):
        choice = random.choice(['red', 'blue'])
        for key in team_mapper.keys():
            if key == choice:
                team_mapper.get(key).append(Solider())

    len_red_team = len(team_red)
    len_blue_team = len(team_blue)
    print(f'Red team have {len_red_team} soliders')
    print(f'Blue team have {len_blue_team} soliders')
    if len_red_team > len_blue_team:
        hero_red.level_up()
    elif len_red_team < len_blue_team:
        hero_blue.level_up()

    random.choice(team_red).move_to_hero(hero_red)

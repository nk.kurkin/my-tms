from functools import wraps
from time import perf_counter


# Функция декоратор, который печатает имя функцкии и все аргументы
def dec_func_name_args(func):
    @wraps
    def wrapper(*args):
        print(func.__name__)
        print(f"args: {args}")
    return wrapper


# 1. Функция декоратор, который печатает перед и после вызова функции слова
# “Before” and “After”
def dec_before_after(func):
    @wraps(func)
    def wrapper(*args):
        print('Before')
        solution = func(*args)
        print('After')
        return solution
    return wrapper


# 2. Функция декоратор, которая добавляет 1 к заданному числу функции
def dec_increment(func):
    @wraps(func)
    def wrapper(*args):
        args = tuple([i + 1 for i in args])
        return func(*args)
    return wrapper


# 3. Функция декоратор, которая переводит полученный текст в верхний регистр
def dec_upper(func):
    @wraps(func)
    def wrapper(arg):
        return func(arg).upper()
    return wrapper


# 4. Функция декоратор func_name, которая выводит на экран имя
# функции (func.__name__)
def func_name(func):
    @wraps(func)
    def wrapper(*args):
        print(func.__name__)
        return func(*args)
    return wrapper


# 5. Напишите декоратор change, который делает так, что задекорированная
# функция принимает все свои не именованные аргументы в порядке, обратном
# тому, в котором их передали
def change(func):
    @wraps(func)
    def wrapper(*args):
        args = args[::-1]
        return func(*args)
    return wrapper


# 6. Напишите декоратор, который вычисляет время работы декорируемой функции
# (timer)
def timer(func):
    @wraps(func)
    def wrapper(*args):
        start_time = perf_counter()
        solution = func(*args)
        end_time = perf_counter()
        print(f"Function ends in {end_time - start_time}")
        return solution
    return wrapper


# 9. Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал:
#
# @typed(type=’str’)
# def add_two_symbols(a, b):
#     return a + b
#
# add_two_symbols("3", 5) -> "35"
# add_two_symbols(5, 5) -> "55"
# add_two_symbols('a', 'b') -> 'ab’
#
# @typed(type=’int’)
# def add_three_symbols(a, b, с):
#     return a + b + с
#
# add_three_symbols(5, 6, 7) -> 18
# add_three_symbols("3", 5, 0) -> 8
# add_three_symbols(0.1, 0.2, 0.4) -> 0.7000000000000001


def tiped(typ="str"):
    def inner(func):
        @wraps(func)
        def wrapper(*args):
            args_str = [i for i in args]
            if typ == "str":
                return "".join([str(elem) for elem in args_str])
            elif typ == "int":
                float_or_int = [type(i) == int or type(i) == float for i in
                                args_str]
                if all(float_or_int):
                    return func(*args)
            else:
                print("Неправильно введынные аргументы")
        return wrapper
    return inner

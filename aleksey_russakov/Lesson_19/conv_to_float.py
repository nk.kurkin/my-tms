"""
переводит переданное число в тип float, если передана строка вызывает
пользовательскую ошибку InputTypeError (создать ее самому)
"""


class InputTypeError(Exception):
    def __init__(self, message='Impossible to convert string to float!'):
        super().__init__(message)


def to_float(number):
    if isinstance(number, str):
        raise InputTypeError
    return float(number)


if __name__ == '__main__':
    print(to_float("3"))

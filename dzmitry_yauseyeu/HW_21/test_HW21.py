import time

from selenium.webdriver.common.by import By
import pytest
from selenium import webdriver

URL = 'http://the-internet.herokuapp.com/'


@pytest.fixture()
def driver():
    chrome_driver \
        = webdriver.\
        Chrome(executable_path="C:/Dim/chromedriver_win32/chromedriver")
    chrome_driver.get(URL)
    return chrome_driver


def checkbox_checked(checkbox):
    is_checked = checkbox.get_attribute("checked")
    if is_checked == "true":
        return True
    return False


# Задание 1
# На главной странице перейти по ссылке Checkboxes
# Установить галочку для второго чекбокса и
# проверить что она установлена
# Для первого чекбокса убрать галочку и протестировать, что ее нет

def test_checkbox2_checked(driver):
    search_field = driver.find_element(By.LINK_TEXT, "Checkboxes")
    search_field.click()
    search_field = driver.find_element(By.XPATH,
                                       "//*[@id=\"checkboxes\"]/input[2]")
    search_field.click()
    result = checkbox_checked(search_field)
    assert result is False


def test_checkbox1_checked(driver):
    search_field = driver.find_element(By.LINK_TEXT, "Checkboxes")
    search_field.click()
    search_field = driver.find_element(By.XPATH,
                                       "//*[@id=\"checkboxes\"]/input[1]")
    search_field.click()
    result = checkbox_checked(search_field)
    assert result is True


# Задание 2
# На главной странице перейти по ссылке Multiple Windows
# После этого найти и нажать на ссылку Click here
# Убедиться, что открылось новое окно и проверить
# что заголовок страницы –
# New window


def test_new_tab(driver):
    driver.find_element(By.XPATH,
                        "//*[text()='Multiple Windows']").click()
    driver.find_element(By.XPATH,
                        "//*[text()='Click Here']").click()
    new_tab = driver.window_handles[1]
    driver.switch_to.window(new_tab)
    new_tab_text = driver.find_element(By.XPATH,
                                       "//h3[text()='New Window']").text
    assert new_tab_text == 'New Window'
    tab_title = driver.title
    assert tab_title == 'New Window'


# Задание 3
# На главной странице перейти по ссылке Add/Remove elements
# Проверить что при нажатии на кнопку Add element,
# появляется новый элемент
# Написать отдельный тест, который проверяет удаление элементов


def test_element_add_remove(driver):
    driver.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    time.sleep(2)
    driver.find_element(By.XPATH,
                        "//*[@id=\"content\"]/div/button").click()
    time.sleep(3)
    result = driver.find_element(By.XPATH,
                                 "//*[@id=\"elements\"]/button[1]") \
        .is_displayed()
    assert result is True


def test_element_was_removed(driver):
    driver.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    time.sleep(2)
    driver.find_element(By.XPATH, "//*[text()='Add Element']").click()
    time.sleep(3)
    driver.find_element(By.XPATH, "//*[text()='Delete']").click()
    result_of_removed = driver.find_elements(By.XPATH, "//*[text()='Delete']")
    assert len(result_of_removed) == 0

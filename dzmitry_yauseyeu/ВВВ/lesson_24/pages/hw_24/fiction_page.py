import time

from base_page_hw import BasePage
from books_locators import BookPageLocators
from fiction_locator import FictionPageLocators


class FictionPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_fiction(self):
        books = self.driver.find_element(*BookPageLocators.BOOKS_LINK)
        books.click()
        time.sleep(2)
        fiction = self.driver.\
            find_element(*FictionPageLocators.FICTION_LINK)
        fiction.click()

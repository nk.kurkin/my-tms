from base_page_hw import BasePage


class MainPage(BasePage):

    URL = "http://selenium1py.pythonanywhere.com/en-gb/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

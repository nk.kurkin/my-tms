"""Типы данных:
1. Переменной var_int присвойте значение 10, var_float - значение 8.4,
var_str - "No".
2. Измените значение, хранимое в переменной var_int, увеличив его в 3.5 раза,
 результат свяжите с переменной big_int.
3. Измените значение, хранимое в переменной var_float, уменьшив
 на единицу, результат свяжите с той же переменной.
4. Разделите var_int на var_float, а затем big_int на var_float.
Результат данных выражений не привязывайте ни к каким переменным.
5. Измените значение переменной var_str на "NoNoYesYesYes".
 При формировании нового значения используйте операции конкатенации (+)
 и повторения строки (*).
 6. Выведите значения всех переменных.
"""
var_int = 10
var_float = 4.8
var_str = "No"
big_int = var_int * 3.5
var_float += 1
new_str = (var_str * 2) + ("Yes" * 3)

int_del_on_float = var_int / var_float
big_int_del_on_float = big_int / var_float

print(big_int)
print(var_float)
print(int_del_on_float)
print(big_int_del_on_float)
print(new_str)

"""1.Свяжите переменную с любой строкой, состоящей не менее чем из 8
символов.  Извлеките из строки первый символ, затем последний,
третий с начала и третий с конца. Измерьте длину вашей строки."""

str_ = "Daw is here already"
print("first letter", str_[0])
print("last letter", str_[-1])
print("third letter", str_[2])
print("third from the end letter", str_[-3])
print("Full length", len(str_))

""" Присвойте произвольную строку длиной 10-15
символов переменной и извлеките из нее следующие срезы:
 - первые восемь символов
 - четыре символа из центра строки
 - символы с индексами кратными трем
 - переверните строку
"""

str_1 = "Daw is here already"
middle = int((len(str_1) / 2))  # middle of string

print(" first of 8 =", str_[:8])
print("4 from middle =", str_1[middle - 2:middle + 2])
print("where step of 3 =", str_1[::3])
print("revers =", str_1[::-1])

"""Есть строка: “my name is name”. Напечатайте ее,
но вместо 2ого “name” вставьте ваше имя."""
str_3 = "My name is name"

words = str_3.split()
words[-1] = "DIma"
print(' '.join(words))
# or
# print(str_3.replace("is name", "is DIma"))

""". Есть строка: test_1 = "Hello world!", необходимо
- напечатать на каком месте находится буква w
- кол-во букв l
- Проверить начинается ли строка с подстроки: “Hello”
- Проверить заканчивается ли строка подстрокой: “qwe”
"""
test_1 = "Hello world!"

print("place of W = ", test_1.index('w'))
print("count of L = ", test_1.count('l'))
print("start from Hello = ", test_1.startswith("Hello"))
print("end on QWE = ", test_1.endswith("qwe"))

import random
"""Task#1:
Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
"""
web_side = "www.my_site.com#about"
new_bed_side = web_side.replace("#", "/")
print(new_bed_side)

"""Task#2:
В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"""

full_name = "Ivanou Ivan"
new_full_name = full_name.split(' ')
new1_full_name = new_full_name[::-1]
new2_full_name = ' '.join(new1_full_name)
print(new2_full_name)

"""Task#3:
Напишите программу которая удаляет пробел в начале строки"""
text = " Напишите программу которая "
without_space_leading = text.lstrip()
print(without_space_leading)

"""Task#4:
Напишите программу которая удаляет пробел в конце строки"""
text = " Напишите программу которая "
without_space_trailing = text.rstrip()
print(without_space_trailing)

"""Task#5:
a = 10, b=23, поменять значения местами, чтобы в переменную
“a” было записано значение “23”, в “b” - значение “-10”"""

a, b = 10, 23
a, b = b, -a
print("a:", a, "b:", b)

"""Task#6:
значение переменной “a” увеличить в 3 раза,
а значение “b” уменьшить на 3"""

a *= 3  # more in 3 times
b -= 3  # low on 3 times

print("a:", a, "b:", b)

"""Task#7:
преобразовать значение “a” из целочисленного в число
с плавающей точкой (float), а значение в переменной “b” в строку"""

float_a = float(a)
str_b = str(b)
print(type(float_a), float_a)
print(type(str_b), str_b)

"""Task#8:
Разделить значение в переменной “a” на
11 и вывести результат с точностью 3 знака после запятой"""

division = a / 11
print(round(division, 3))

"""Task#9:
Преобразовать значение переменной “b” в число с плавающей точкой и
записать в переменную “c”. Возвести полученное число в 3-ю степень."""

float_b = float(b)
pow_b = pow(float_b, 3)
print(pow_b)

"""Task#9:
Получить случайное число, кратное 3-м """
print(random.randrange(10, 20, 3))

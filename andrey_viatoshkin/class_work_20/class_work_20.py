import time
import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


URL = 'https://ru.wikipedia.org/wiki/Main_Page/'
SEARCH_INPUT_ID = 'searchInput'
SEARCH_RESULT_XPATH = '//div/a[@title="Python"]'
SEARCH_HEADER = 'firstHeading'


@pytest.fixture(scope='module')
def browser():
    driver = webdriver.Chrome()
    driver.get(URL)
    yield driver
    time.sleep(2)
    driver.quit()


def test_find_element(browser):
    search_field = browser.find_element(By.ID, SEARCH_INPUT_ID)
    search_field.send_keys('Python')
    search_field.send_keys(Keys.ENTER)
    browser.find_element(By.XPATH,
                         SEARCH_RESULT_XPATH).click()
    search_header = browser.find_element(By.ID, SEARCH_HEADER)
    assert search_header.text == 'Python', 'Page not found'

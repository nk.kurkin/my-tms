import random
# В классическом варианте игра рассчитана на двух игроков. Каждый из
# игроков задумывает и записывает тайное 4-значное число с неповторяющимися
# цифрами. Игрок, который начинает игру по жребию, делает первую попытку
# отгадать число. Попытка — это 4-значное число с неповторяющимися цифрами,
# сообщаемое противнику. Противник сообщает в ответ, сколько цифр угадано
# без совпадения с их позициями в тайном числе (то есть количество коров)
# и сколько угадано вплоть до позиции в тайном числе (то есть количество
# быков). При игре против компьютера игрок вводит комбинации одну за
# другой, пока не отгадает всю последовательность.
# Ваша задача реализовать программу, против которой можно сыграть
# в "Быки и коровы"
cows = int()
bulls = int()
num_1 = random.sample(range(10), 4)
print(num_1)


while cows != 0 or bulls != 4:
    answer = input('ведите 4 цифры ')
    answer = [int(x) for x in str(answer)]
    cows = int()
    bulls = int()
    for i in range(len(answer)):
        if answer[i] == num_1[i]:
            bulls = bulls + 1
        elif answer[i] in num_1:
            cows = cows + 1

    if cows == 0 and bulls == 4:
        print('Вы выиграли!')
    elif cows != 1 and bulls == 1:
        print(f'{cows} коровы и {bulls} бык')
    elif cows == 1 and bulls != 1:
        print(f'{cows} корова и {bulls} быка')
    else:
        print(f'{cows} коровы и {bulls} быка')

# Like
# Создайте программу, которая, принимая массив имён, возвращает строку
# описывающая количество лайков (как в Facebook).
#
# Примеры:
# Введите имена через запятую: "Ann"
# -> "Ann likes this"
# Введите имена через запятую: "Ann, Alex"
# -> "Ann and Alex like this"
# Введите имена через запятую: "Ann, Alex, Mark"
# -> "Ann, Alex and Mark like this"
# Введите имена через запятую: "Ann, Alex, Mark, Max"
# -> "Ann, Alex and 2 others like this"
# Если ничего не вводить должен быть вывод:
# -> "No one likes this"
names = input('Введите имена через запятую (например "Ann, Alex") ')
name_list = names.split(',')


if name_list[0] == '':
    print('No one likes this')
elif len(name_list) == 1:
    print(f'{name_list[0]} likes this')
elif len(name_list) == 2:
    print(f'{name_list[0]} and {name_list[1]} like this')
elif len(name_list) == 3:
    print(f'{name_list[0]} and {name_list[1]} and {name_list[2]} like '
          f'this')
elif len(name_list) == 4:
    print(f'{name_list[0]} and {name_list[1]} and {name_list[2]} and'
          f' {name_list[3]} like this')
else:
    print(f'{name_list[0]}, {name_list[1]} and 2 others like this')

# Напишите программу, которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа, а
# для чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5 надо
# печатать "FuzzBuzz". Иначе печатать число.

row_of_numbers = list(range(1, 101))


for i in row_of_numbers:
    if i % 3 == 0 and i % 5 == 0:
        print('FuzzBuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)

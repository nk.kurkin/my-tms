from selenium.webdriver.common.by import By


class BasketPageLocators:
    EMPTY_BIN_MESSAGE_SELECTOR = (By.XPATH, '//*[@id="content_inner"]')
    BASKET_HEADER = (By.XPATH, '//h1')

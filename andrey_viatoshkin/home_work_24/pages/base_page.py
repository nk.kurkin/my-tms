from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open_site(self):
        self.driver.get(self.url)

    def element_is_presented(self, locator, timeout=10):
        return WebDriverWait(self.driver, timeout).until(
            EC.presence_of_element_located(locator)
        )

    def element_is_clickable(self, locator, timeout=20):
        return WebDriverWait(self.driver, timeout).until(
            EC.element_to_be_clickable(locator)
        )

    def get_page_url(self):
        return self.driver.current_url

    def get_title(self):
        return self.driver.title

    def get_header(self, class_name_locator):
        return self.driver.find_element(*class_name_locator).text

    def open_page(self, locator, class_name):
        self.driver.find_element(*locator).click()
        return class_name(self.driver)

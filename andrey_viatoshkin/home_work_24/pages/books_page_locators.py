from selenium.webdriver.common.by import By


class BooksPagesLocators:
    HEADER_SELECTOR = (By.XPATH, '//h1[text()="Books"]')
    FICTION_LINK_SELECTOR = (By.XPATH, '//*[@class="side_categories"]'
                                       '//a[contains(@href,"fiction_3")]')
    BOOK_CARDS = (By.XPATH, '//a[@title]')

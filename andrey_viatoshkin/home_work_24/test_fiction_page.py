from andrey_viatoshkin.home_work_24.pages.fiction_page_locators import \
    FictionPageLocators
from andrey_viatoshkin.home_work_24.pages.fiction_page import FictionPage
from andrey_viatoshkin.home_work_24.pages.books_page_locators import\
    BooksPagesLocators


def test_header(driver, return_book_page):
    fiction_page = return_book_page.open_page(
        BooksPagesLocators.FICTION_LINK_SELECTOR, FictionPage)
    assert 'Fiction' == fiction_page.get_header(
        FictionPageLocators.HEADER_SELECTOR)


def test_tab_name(driver, return_book_page):
    fiction_page = return_book_page.open_page(
        BooksPagesLocators.FICTION_LINK_SELECTOR, FictionPage)
    assert 'Fiction | Oscar - Sandbox' == fiction_page.get_title(), 'Wrong tab'


def test_page_url(driver, return_book_page):
    fiction_page = return_book_page.open_page(
        BooksPagesLocators.FICTION_LINK_SELECTOR, FictionPage)
    assert 'fiction_3/' in fiction_page.get_page_url(), 'Wrong URL'

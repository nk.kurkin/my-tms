import pytest
from check_letter_case import check_letter_case
from check_special_character import check_special_character
from convert_to_float import convert_to_float, InputTypeError


@pytest.mark.repeat(3)
@pytest.mark.parametrize('test_str, expected', [
    ('test', True),
    ('Test', False),
    ('123', False),
    (123, AttributeError),
    (None, AttributeError)])
def test_check_letter_case(test_str, expected):
    if isinstance(expected, type) and issubclass(expected, Exception):
        with pytest.raises(expected):
            check_letter_case(test_str)
    else:
        assert check_letter_case(test_str) == expected


@pytest.mark.repeat(3)
@pytest.mark.parametrize('character, expected', [
    ('test', False),
    ('!', True),
    (123, TypeError),
    ('!qwer', False),
    (None, TypeError)
])
def test_check_special_character(character, expected):
    if isinstance(expected, type) and issubclass(expected, Exception):
        with pytest.raises(expected):
            check_special_character(character)
    else:
        assert check_special_character(character) == expected


@pytest.mark.repeat(3)
@pytest.mark.parametrize('number, expected', [
    ('test', InputTypeError),
    ('!', InputTypeError),
    (123, 123.0),
    ('!qwer', InputTypeError),
    (None, InputTypeError)
])
def test_convert_to_float(number, expected):
    if isinstance(expected, type) and issubclass(expected, Exception):
        with pytest.raises(expected):
            convert_to_float(number)
    else:
        assert convert_to_float(number) == expected


def test_input_type_error():
    with pytest.raises(InputTypeError):
        convert_to_float('2')

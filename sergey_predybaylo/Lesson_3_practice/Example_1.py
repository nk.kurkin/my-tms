var_int = 10
var_float = 8.4
var_str = "No"
big_int = var_int * 3.5
var_float -= 1

print(var_int / var_float)
print(big_int / var_float)

a = "No"
b = "Yes"
var_str = a + a + b * 3  # использование конкатенации и повторения строк
print(var_str, var_float, var_int, big_int)  # вывод всех значений

"""Строки. Задание 1"""
random_str = "zxcvasdf"
print(
    random_str[0], random_str[-1],
    random_str[2], random_str[-3]
)  # извлечение рандомных символов из строки
print(len(random_str))

""""Задание 2"""
str1 = "I want go home!"
print(str1[:8])  # извлечение первых 8 символов
print(str1[2:6])  # извлечение 4 символов из центра строки
print(str1[3::3])  # извлечение символов с индексом кратным 3
print(str1[::-1])  # перевертывание строки

""""Задание 3"""
m_name = "Sergey"
print(f"My name is {m_name}")  # Подставляем имя

""""Задание 4"""
test_tring = "Hello, World!"

print("Позиция символа W =", test_tring.find("W"))  # Поиск символа "W"
print("Количество символа l = ", test_tring.count("l"))  # Подсчет символа "l"
print(test_tring.startswith("Hello"))
print(test_tring.endswith("qwe"))

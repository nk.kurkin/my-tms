from selenium.webdriver.common.by import By


class FictionPageLocators:
    FICTION_PAGE_SUB = (By.XPATH, '//*[@class="page-header action"]/h1')
    FICTION_LINK = (
        By.CSS_SELECTOR, '.dropdown-submenu>.dropdown-menu>:nth-child(1)>a')

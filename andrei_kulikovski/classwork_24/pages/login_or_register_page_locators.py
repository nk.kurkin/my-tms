from selenium.webdriver.common.by import By


class LoginOrRegisterPageLocators:
    LOGIN_FORM = (By.ID, "login_form")
    REGISTER_FORM = (By.ID, "register_form")


class LoginFormLocators:
    EMAIL_ADDRESS = (By.CSS_SELECTOR, "input[name=login-username]")

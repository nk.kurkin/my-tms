"""
⦁	Написать игру крестики нолики

В этой игре человек играет против компьютера.
Изначально компьютер рисует поле на котором будете играть, после чего просит
выбрать, за какую команду играет человек: 0 или Х
И дальше сделать ход выбрав координаты.
После чего компьютер делает ход и рисует обновленное поле

       |    |
-----|---|-----
       |    |
-----|---|-----
       |    |
Выберите команду:
1) Крестики
2) Нолики
1

Введите координаты: 1, 3
       |    | X
-----|---|-----
       | 0 |
-----|---|-----
       |    |
Игра продолжается до последнего хода, либо пока кто то не победит
Выход из программы должен производиться пользователем через опцию
Игра окончена:
А) Сыграть еще одну партию
Б) Выйти из программы
"""
x = 'x'
o = '0'
size_board = 9
moves = ' '
draw = 'Ничья'


def instr():
    """shows the numbering of the field cells"""
    print('''
Карта игрового поля

0 | 1 | 2
---------
3 | 4 | 5
---------
6 | 7 | 8
''')


def start(question):
    """start game choice x or 0"""
    answer = None
    while answer not in ('x', '0'):
        answer = input(question).lower()
    return answer


def x_or_o():
    """who walks first"""
    one_step = start("Выберите команду x или 0: ")
    if one_step == 'x':
        print('ты играешь крестиками')
        human = x
        comp = o
    else:
        print('я делаю первый ход крестиками')
        human = o
        comp = x
    return comp, human


def step_number(low, high):
    """selection of position number on the board"""
    answer = None
    while answer not in range(low, high):
        answer = int(input("Делай свой ход - напиши номер поля (0-8): "))
    return answer


def new_board():
    """creates a board with a marked position"""
    board = []
    for i in range(size_board):
        board.append(moves)
    return board


def show_board(board):
    """outputting a board with a marked position"""
    print('\n', board[0], '|', board[1], '|', board[2])
    print('-----------')
    print('\n', board[3], '|', board[4], '|', board[5])
    print('-----------')
    print('\n', board[6], '|', board[7], '|', board[8], '\n')


def available_moves(board):
    """remaining options for moves"""
    available_move = []
    for i in range(size_board):
        if board[i] == moves:
            available_move.append(i)
    return available_move


def winner(board):
    """winning combinations"""
    var_win = ((0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
               (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6))
    for i in var_win:
        if board[i[0]] == board[i[1]] == board[i[2]] != moves:
            return board[i[0]]
        if moves not in board:
            return draw
    return None


def human_mov(board, human):
    """a person walks if the field is busy displaying a message"""
    variants_mov = available_moves(board)
    step = None
    while step not in variants_mov:
        step = step_number(0, size_board)
        if step not in variants_mov:
            print('Поле занято. Напиши другой номер: ')
    print('Супер!')
    return step


def comp_mov(board, comp, human):
    """the computer looks through the remaining options for the move,
     and there is a winning combination for a person or a computer"""
    board = board[:]
    print('Мой ход: ')
    for i in available_moves(board):
        board[i] = comp
        if winner(board) == comp:
            print(i)
            return i
        board[i] = moves
    for j in available_moves(board):
        board[j] = human
        if winner(board) == human:
            print(j)
            return j
        board[j] = moves
    for k in available_moves(board):
        print(k)
        return k


def next_queue(queue):
    """determines whose move"""
    if queue == x:
        return o
    else:
        return x


def congratulations(winn, comp, human):
    """displaying the message who win: human versus computer or a draw"""
    if winn != draw:
        print('Собрана линия ', winn)
    else:
        print(draw)
    if winn == comp:
        print('Компьютер выиграл!')
    elif winn == human:
        print('Ты победил!')
    elif winn == draw:
        print(draw)


def main():
    """main function program structure game, next restart or exit"""
    instr()
    comp, human = x_or_o()
    queue = x
    board = new_board()
    show_board(board)
    while not winner(board):
        if queue == human:
            step = human_mov(board, human)
            board[step] = human
        else:
            step = comp_mov(board, comp, human)
            board[step] = comp
        show_board(board)
        queue = next_queue(queue)
    winn = winner(board)
    congratulations(winn, comp, human)


main()

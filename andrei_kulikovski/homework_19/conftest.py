import pytest
from datetime import datetime


@pytest.fixture(autouse=True, scope="session")
def name(request):
    print(request.node.name)


@pytest.fixture(autouse=True, scope="session")
def time():
    start = datetime.now()
    print("START TEST", start)
    yield start
    stop = datetime.now()
    print("STOP TEST", stop)


@pytest.fixture(autouse=True)
def check_time_and_request(time, request):
    start_check = datetime.now()
    delta = start_check - time
    print(request.fixturenames)
    print(delta)

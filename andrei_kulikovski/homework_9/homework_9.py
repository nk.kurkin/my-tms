"""
Библиотека
Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
зарезервирована ли книги или нет. Создайте класс пользователь который может
брать книгу, возвращать, бронировать. Если другой пользователь хочет взять
зарезервированную книгу(или которую уже кто-то читает - надо ему про это
сказать).
"""


class Book:
    book_num = []

    def __init__(self, name, author, pages, isbn, reserved=False):
        self.name = name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.reserved = reserved


class User:

    def __init__(self, name):
        self.name = name

    def take_book(self, book):
        if book.reserved:
            print('book reserve or taken')
        else:
            book.reserved = True
            book.book_num = self.name
            print('take your book')
        return book.book_num

    def return_book(self, book):
        if self.name == book.book_num and book.reserved:
            book.reserved = False
            print('book returned')
        return book.reserved

    def reserved_book(self, book):
        if book.reserved:
            print('book reserve or taken')
        else:
            book.reserved = True
            book.book_num = self.name
            print('your reserve')
        return book.book_num


book_1 = Book('book1', 'author1', 500, 3131)
book_2 = Book('book2', 'author2', 400, 3434)
user_1 = User('Oleg')
user_2 = User('Ivan')

user_1.take_book(book_1)
user_2.take_book(book_1)
user_1.return_book(book_1)
user_2.reserved_book(book_2)
user_1.reserved_book(book_2)


"""
Банковский вклад
Создайте класс инвестиция. Который содержит необходимые поля и методы,
например сумма инвестиция и его срок.
Пользователь делает инвестиция в размере N рублей сроком на R лет под 10%
годовых (инвестиция с возможностью ежемесячной капитализации - это означает,
что проценты прибавляются к сумме инвестиции ежемесячно).
Написать класс Bank, метод deposit принимает аргументы N и R, и возвращает
сумму, которая будет на счету пользователя.
"""


class Inv:
    def __init__(self, sum_inv, time_inv):
        self.sum_inv = sum_inv
        self.time_inv = time_inv


class Bank(Inv):
    def deposit(self):
        for i in range(self.time_inv):
            self.sum_inv = self.sum_inv + (self.sum_inv * 0.10)
        return self.sum_inv


bank1 = Bank(2000, 2)
print(bank1.deposit())

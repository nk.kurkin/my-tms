from decorators import before_and_after, decor_sum, decor_upper, func_name,\
    change, timer, typed


"""
Напишите декоратор, который печатает перед и после вызова функции слова
“Before” and “After”
"""


@before_and_after
def func_text():
    """
    printing string
    """
    print("text")


func_text()


"""
Напишите функцию декоратор, которая добавляет 1 к заданному числу
"""


@decor_sum
def func_x(x):
    """
    :param x:
    :return:
    """
    return x


print(func_x(5))


"""
Напишите функцию декоратор, которая переводит полученный текст в
верхний регистр
"""


@decor_upper
def func_hello(text):
    """
    :param text:
    :return:
    """
    return text


print(func_hello("hello"))


"""
Напишите декоратор func_name, который выводит на экран имя функции
(func.__name__)
"""


@func_name
def func_4():
    """
    x = "abc" return
    :return:
    """
    x = "abc"
    return x


print(func_4())


"""
Напишите декоратор change, который делает так, что задекорированная функция
принимает все свои не именованные аргументы в порядке, обратном тому,
в котором их передали
"""


@change
def div(x, y):
    """
    :param x: 2
    :param y: 4
    :return: result x / y
    """
    result = x / y
    return result


print(div(2, 4))


"""
Напишите декоратор, который вычисляет время работы декорируемой функции (timer)
"""


@timer
def dive(x, y):
    """
    :param x: 2
    :param y: 4
    :return: result x / y
    """
    result = x / y
    return result


print(dive(2, 4))


"""
Напишите функцию, которая вычисляет значение числа Фибоначчи для заданного
количество элементов последовательности и возвращает его и оберните ее
декоратором timer и func_name
"""


@timer
@func_name
def fib():
    """
    fibonachi count 5
    :return:
    """
    x = 0
    y = 1
    q = 0
    while q < 5:
        print(x)
        q += 1
        x, y = y, x + y


fib()


"""
Напишите функцию, которая вычисляет сложное математическое выражение и
оберните ее декоратором из пункта 1, 2
"""


@before_and_after
@decor_sum
def func_mat(x, y):
    """
    (abs(x) - abs(y)) / (1 + abs(x * y))
    :param x: 6
    :param y: 5
    :return:
    """
    return (abs(x) - abs(y)) * (1 + abs(x * y))


"""
Напишите декоратор, который проверял бы тип параметров функции,
конвертировал их если надо и складывал:
@typed(type=’str’)
def add_two_symbols(a, b):
    return a + b

add_two_symbols("3", 5) -> "35"
add_two_symbols(5, 5) -> "55"
add_two_symbols('a', 'b') -> 'ab’

@typed(type=’int’)
def add_three_symbols(a, b, с):
    return a + b + с

add_three_symbols(5, 6, 7) -> 18
add_three_symbols("3", 5, 0) -> 8
add_three_symbols(0.1, 0.2, 0.4) -> 0.7000000000000001
"""


@typed(type="str")
def add_two_symbols(a, b):
    """
    :param a:
    :param b:
    :return: a + b
    """
    return a + b


@typed(type="int")
def add_three_symbols(a, b, с):
    """
    :param a:
    :param b:
    :param с:
    :return: a + b + с
    """
    return a + b + с

"""
Суть задания:
На ферме могут жить животные и птицы
И у тех и у других есть два атрибута: имя и возраст
При этом нужно сделать так, чтобы возраст нельзя было изменить извне напрямую,
но при этом можно было бы получить его значение
У каждого животного должен быть обязательный метод – ходить
А для птиц – летать
Эти методы должны выводить сообщения (контекст придумайте)
Также должны быть общие для всех классов обязательные методы – постареть -
который увеличивает возраст на 1 и метод голос

При обращении к несуществующему методу или атрибуту животного или птицы должен
срабатывать пользовательский exception NotExistException
и выводить сообщение о том, что для текущего класса (имя класса),
такой метод или атрибут не существует – (имя метода или атрибута)

На основании этих классов создать отдельные классы для Свиньи, Гуся,
Курицы и Коровы – для каждого класса метод голос должен выводить разное
сообщение (хрю, гага, кудах, му)

После этого создать класс Ферма, в который можно передавать список животных
Должна быть возможность получить каждое животное, живущее на ферме как
по индексу, так и через цикл for.
Также у фермы должен быть метод, который увеличивает возраст всех животных,
живущих на ней.
"""

from abc import ABC, abstractmethod


class Animal(ABC):
    @abstractmethod
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __getattr__(self, name):
        raise NotExistException(f'{self.__class__.__name__}'
                                f' no attribute {name}')

    @abstractmethod
    def old(self):
        print(self.age + 1)

    @abstractmethod
    def move(self):
        print("move")

    @abstractmethod
    def voice(self):
        print("voice")


class Bird(ABC):
    @abstractmethod
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __getattr__(self, name):
        raise NotExistException(f'{self.__class__.__name__}'
                                f' no attribute {name}')

    @abstractmethod
    def old(self):
        print(self.age + 1)

    @abstractmethod
    def fly(self):
        print("fly")

    @abstractmethod
    def voice(self):
        print("voice")


class Pig(Animal):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def move(self):
        print("step")

    def voice(self):
        print("хрю")


class Cow(Animal):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def move(self):
        print("step")

    def voice(self):
        print("му")


class Goose(Bird):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def fly(self):
        print("fly")

    def voice(self):
        print("гага")


class Chicken(Bird):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def fly(self):
        print("fly")

    def voice(self):
        print("ку-дах")


class Farm:
    def __init__(self, *args):
        self.args = args

    def __getitem__(self, index):
        return self.args[index]

    def __iter__(self):
        return iter(self.args)

    def older(self, *args):
        for beast in args:
            beast.age += 3


pig1 = Pig("pig", 3)
pig1.voice()
pig1.old()
pig1.move()

cow1 = Cow("cow", 6)
cow1.voice()
cow1.old()
cow1.move()

goose1 = Goose("goose", 4)
goose1.voice()
goose1.old()
goose1.fly()

chicken1 = Chicken("chicken", 2)
chicken1.voice()
chicken1.old()
chicken1.fly()

farm = Farm(pig1, cow1, goose1, chicken1)
print(pig1.name)
print(cow1.name)
print(goose1.name)
print(chicken1.name)
for f in farm:
    print(f.name)
farm.older(pig1, cow1, goose1, chicken1)
print("Pig age", pig1.age)


class NotExistException(Exception):
    pass


pig1.color()

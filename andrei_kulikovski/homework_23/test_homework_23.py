import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


CHECKBOX = '//*[@id="checkbox-example"]/input'
BUTTON_CHECKBOX = '//*[@id="checkbox-example"]/button'
MESSAGE_CHECKBOX = '//*[@id="checkbox-example"]/p[@id="message"]'

INPUT = '//*[@id="input-example"]/input'
BUTTON_INPUT = '//*[@id="input-example"]/button'
MESSAGE_INPUT = '//*[@id="input-example"]/p[@id="message"]'

IFRAME = '//*[text()="iFrame"]'
TEXT_IFRAME = '//*[@id="tinymce"]/p'


def test_dynamic_controls(driver):
    driver.find_element(By.LINK_TEXT, "Dynamic Controls").click()
    driver.find_element(By.XPATH, BUTTON_CHECKBOX).click()
    WebDriverWait(driver, 5).until(EC.presence_of_element_located
                                   ((By.XPATH, MESSAGE_CHECKBOX)))
    message_text = driver.find_element(By.XPATH, MESSAGE_CHECKBOX).text
    assert message_text == "It's gone!"
    with pytest.raises(NoSuchElementException):
        driver.find_element(By.XPATH, CHECKBOX)
    assert not driver.find_element(By.XPATH, INPUT).is_enabled()
    driver.find_element(By.XPATH, BUTTON_INPUT).click()
    WebDriverWait(driver, 5).until(EC.presence_of_element_located
                                   ((By.XPATH, MESSAGE_INPUT)))
    message_text = driver.find_element(By.XPATH, MESSAGE_INPUT).text
    assert message_text == "It's enabled!"
    assert driver.find_element(By.XPATH, INPUT).is_enabled()


def test_iframe(driver):
    driver.find_element(By.LINK_TEXT, "Frames").click()
    driver.find_element(By.XPATH, IFRAME).click()
    driver.switch_to.frame(driver.find_element(By.TAG_NAME, "iframe"))
    WebDriverWait(driver, 5).until(EC.presence_of_element_located
                                   ((By.XPATH, TEXT_IFRAME)))
    iframe_text = driver.find_element(By.XPATH, TEXT_IFRAME).text
    assert iframe_text == "Your content goes here."

"""
Путешествие
Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
Класс студен описан следующим образом:
class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money
Необходимо понять у кого больше всего денег и вернуть его имя.
assert most_money([ivan, oleg, sergei]) == "sergei"
assert most_money([ivan, oleg]) == "ivan"
assert most_money([oleg]) == "Oleg"
"""


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

    @property
    def get_money(self):
        return self.money

    @get_money.setter
    def get_money(self, money):
        self.money = money


def most_money(arg):
    student_money = [student.money for student in arg]
    max_money = max(student_money)
    if student_money.count(max_money) == len(student_money):
        return "all"
    for student in arg:
        if student.money == max_money:
            return student.name


if __name__ == "__main__":
    print(f"task_1 is {__name__}")
else:
    print(f"import {__name__}")
ivan = Student("ivan", 220)
ivan.get_money = 300
oleg = Student("oleg", 280)
sergei = Student("sergei", 220)

assert most_money([ivan, oleg, sergei]) == "ivan"
print(most_money([ivan, oleg, sergei]))

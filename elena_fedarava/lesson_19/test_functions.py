import pytest
from func1 import verify_lower_case
from func2 import convert_to_float
from func3 import is_spec_symbol


@pytest.mark.parametrize("test_input,expected",
                         [('test', True),
                          ('TEST', False),
                          ('TeSt', False)])
def test_parametrized_func1(test_input, expected):
    assert verify_lower_case(test_input) == expected


def test_type_error():

    with pytest.raises(TypeError):
        convert_to_float('test')


@pytest.mark.parametrize("test_input,expected",
                         [(12, 12.0),
                          (2.4, 2.4),
                          (0, 0.0)])
def test_parametrized_func2(test_input, expected):
    assert convert_to_float(test_input) == expected


@pytest.mark.parametrize("test_input,expected",
                         [('t', False),
                          ('%', True),
                          ('2', False)])
def test_parametrized_func3(test_input, expected):
    assert is_spec_symbol(test_input) == expected

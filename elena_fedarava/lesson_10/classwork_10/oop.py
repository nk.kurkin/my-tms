# ООП. Задание 1
#
# Создать класс Goods, в котором содержиться атрибуты: price, discount.
# и метод set_discount
#
# Унаследовать класс Food и Tools от Goods
# Унаследовать класс Banana, Apple, Cherry от класса Food
# Унаследовать класс Ham, Nail, Axe от класса Tools
#
# Создать класс Store и добавить в него словарь, где будет храниться
# item: price.
# Реализовать в классе методы add_item, remove_item
# Реализовать метод overall_price_discount который считает полную сумму со
# скидкой добавленных в магазин товаров
# Реализовать метод overall_price_no_discount
# который считает сумму товаров без скидки.
#
# Реализовать классы GroceryStore и HardwareStore и унаследовать эти классы от
# Store
#
# Реализовать в GraceryStore проверку, что объект, который был передан в
# класс пренадлежит классу Food
#
# HardwareStore Tools

class Goods:
    def __init__(self, name, price, discount):
        self.name = name
        self.price = price
        self.discount = discount

    def set_discount(self, value):
        self.discount = value


class Food(Goods):
    pass


class Banana(Food):
    pass


class Cherry(Food):
    pass


class Apple(Food):
    pass


class Tools(Goods):
    pass


class Ham(Tools):
    pass


class Nail(Tools):
    pass


class Axe(Tools):
    pass


class Store:
    def __init__(self, shop_name):
        self.shop_items = {}
        self.shop_items_with_discount = {}
        self.shop_name = shop_name

    def add_item(self, item):
        self.shop_items[item.name] = item.price
        self.shop_items_with_discount[item.name] = item.price - item.discount

    def remove_item(self, value):
        del self.shop_items[value.name]
        del self.shop_items_with_discount[value.name]

    def overall_price_no_discount(self):
        overall_price = 0
        for i in self.shop_items.values():
            overall_price += i
        print('overall price', overall_price)

    def overall_price_discount(self):
        overall_price_with_discount = 0
        for i in self.shop_items_with_discount.values():
            overall_price_with_discount += i
        print('overall price', overall_price_with_discount)


class GroceryStore(Store):

    def add_item(self, item):
        if isinstance(item, Food):
            super().add_item(item)
        else:
            print("Opps..This instance doesn't belong to Food class!")


class HardwareStore(Store):

    def add_item(self, item):
        if isinstance(item, Tools):
            super().add_item(item)
        else:
            print("Opps..This instance doesn't belong to Tools class!")


banana1 = Banana('Зеленый банан', 100, 5)
banana2 = Banana('Красный банан', 150, 5)
nail1 = Nail('Строительные гвозди', 200, 25)

banana2.set_discount(10)
nail1.set_discount(30)

grocery_shop = GroceryStore("Gippo")
hardware_shop = HardwareStore("ElectroSila")

grocery_shop.add_item(banana1)
grocery_shop.add_item(banana2)
print(grocery_shop.shop_items)

hardware_shop.add_item(nail1)
print(hardware_shop.shop_items)

grocery_shop.overall_price_discount()
grocery_shop.overall_price_no_discount()

hardware_shop.overall_price_discount()
hardware_shop.overall_price_no_discount()

grocery_shop.remove_item(banana1)
hardware_shop.remove_item(nail1)
print(grocery_shop.shop_items)
print(hardware_shop.shop_items)

hardware_shop.add_item(banana1)
print(hardware_shop.shop_items)

from functions import func1, func2, func3, func4, func5, func6, func7, func8,\
    add_two_symbols, add_three_symbols

selection = int(input('Select a function to execute: \n'
                      '1.Декоратор, который печатает перед и после '
                      'вызова функции слова “Before” and “After” \n'
                      '2.Декоратор, который добавляет 1 к заданному числу \n'
                      '3.Декоратор, переводит полученный текст в верхний '
                      'регистр \n'
                      '4.Декоратор, который выводит на экран имя функции \n'
                      '5.Декоратор делает так, что функция принимает все свои '
                      'неименованные аргументы в обратном порядке \n'
                      '6.Декоратор вычисляет время работы функции \n'
                      '7.Функция вычисляет значение числа Фибоначчи, + '
                      'декораторы timer, func_name \n'
                      '8.Функцию вычисляет сложное мат. выражение, обернута'
                      ' декоратором из пункта 1, 2 \n'
                      '9.Декоратор проверяет тип параметров функции, '
                      'конвертирует их если надо и складывает \n'))
if selection == 1:
    func1()
elif selection == 2:
    func2(12)
elif selection == 3:
    func3('This is string!')
elif selection == 4:
    func4('Hello!')
elif selection == 5:
    func5(1, 2, 3, tree='test')
elif selection == 6:
    func6(2)
elif selection == 7:
    func7(5)
elif selection == 8:
    func8()
elif selection == 9:
    add_two_symbols("3", 5)
    add_two_symbols(5, 5)
    add_two_symbols('a', 'b')
    add_three_symbols(5, 6, 7)
    add_three_symbols("3", 5, 0)
    add_three_symbols(0.1, 0.2, 0.4)
else:
    print('Your input is incorrect!')

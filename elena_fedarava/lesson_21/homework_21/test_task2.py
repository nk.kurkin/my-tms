# Задание 2
# На главной странице перейти по ссылке Multiple Windows
# После этого найти и нажать на ссылку Click here
# Убедиться, что открылось новое окно и проверить что заголовок страницы
# – New window

from selenium.webdriver.common.by import By

MULTIPLE_WINDOWS_MENU = 'a[href*="windows"]'
CLICK_HERE_XPATH = '//a[text()="Click Here"]'


def test_multiple_windows(open_main_page):
    driver = open_main_page
    driver.find_element(By.CSS_SELECTOR, MULTIPLE_WINDOWS_MENU).click()
    driver.find_element(By.XPATH, CLICK_HERE_XPATH).click()
    assert len(driver.window_handles) == 2  # Убедиться, что открылось нов.окно
    driver.switch_to.window(driver.window_handles[1])
    get_title = driver.title
    assert get_title == 'New Window'  # проверить, что заголовок – New window

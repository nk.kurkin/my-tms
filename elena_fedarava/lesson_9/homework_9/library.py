"""Task1: Библиотека
Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
зарезервирована ли книги или нет. Создайте класс пользователь который может
брать книгу, возвращать, бронировать. Если другой пользователь хочет взять
зарезервированную книгу(или которую уже кто-то читает - надо ему про это
сказать).
"""

# В моей имлементации можно забронировать книгу, если сейчас кто то другой ей
# пользуется, но тогда пока этот человек не возьмет и не отдаст книгу обратно,
# другие не смогут ее забронировать.


class Book:
    def __init__(self, name, author, pages_number, ISBN, is_booked, is_in_use):
        self.name = name
        self.author = author
        self.pages_number = pages_number
        self.ISBN = ISBN
        self.is_booked = is_booked  # boolean
        self.is_in_use = is_in_use  # boolean


book1 = Book('Jane Air', 'Charlotte Bronte', 500, '2-266-1156-6', False, False)


class User:

    booker_name = ''

    def __init__(self, reader_name):
        self.reader_name = reader_name

    def take_book(self, book):
        if not book.is_booked and not book.is_in_use:
            # книга не забукана и не в использовании
            book.is_in_use = True
            print(f'Success! {book.name} is taken by you, {self.reader_name}')
        elif book.is_in_use:
            # книга в использовании
            print(f'Sorry, but {book.name} is already taken by '
                  f'another person!')
        elif not book.is_in_use and book.is_booked and \
                self.reader_name == User.booker_name:
            # книга забукана юзером и этот юзер хочет взять ее
            book.is_in_use = True
            book.is_booked = False
            print(f'Success! Booked {book.name} is taken by '
                  f'{self.reader_name}')
        elif not book.is_in_use and book.is_booked:
            # книга забукана юзером и другой юзер хочет взять ее
            print(
                f'Sorry, {book.name} is booked by another person')

    def return_book(self, book):
        book.is_in_use = False
        if User.booker_name == self.reader_name:
            book.is_booked = False
        print(f'You returned {book.name} to the library!')

    def reserve_book(self, book):
        if book.is_booked:
            print(f'{book.name} is already booked!')
        else:
            book.is_booked = True
            User.booker_name = self.reader_name
            print(f'You, {User.booker_name}, have booked {book.name}')


user1 = User('Alex B')
user2 = User('Mary K')

user1.reserve_book(book1)
user2.take_book(book1)
user1.take_book(book1)
user2.reserve_book(book1)
user1.return_book(book1)
user1.reserve_book(book1)

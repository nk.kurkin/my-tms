from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

IMPRESSIVE_RADIOBUTTON = '//label[@for="impressiveRadio"]'


def test_1():
    try:
        browser = webdriver.Chrome()
        browser.get("https://demoqa.com/radio-button")
        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.XPATH,
                                            IMPRESSIVE_RADIOBUTTON))).click()

        res = browser.find_element(By.XPATH, '//span[@class="text-success"]')
        assert res.text == 'Impressive'
    finally:
        browser.quit()


def test_2():
    browser = webdriver.Chrome()
    browser.get("https://jsbin.com/cicenovile/1/edit?html,output")
    try:
        iframe1 = browser.find_element(By.XPATH, '//iframe[@class="stretch"]')
        browser.switch_to.frame(iframe1)
        iframe2 = WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.NAME, 'JS Bin Output ')))
        browser.switch_to.frame(iframe2)
        click_here = browser.find_element(By.XPATH, '//a[text()="Click me!"]')
        click_here.click()
        alert = browser.switch_to.alert
        alert.accept()
        browser.switch_to.default_content()
        assert browser.find_element(By.CSS_SELECTOR,
                                    '#loginbtn').text == "Login or Register"

    finally:
        browser.quit()

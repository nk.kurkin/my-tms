from calculator import find_sum, find_diff, find_div, find_multiple
import unittest


class TestCalculatorSum(unittest.TestCase):

    def test_sum_positive_check_1(self):
        self.assertEqual(find_sum(1, 4), 5)

    def test_sum_positive_check_2(self):
        self.assertEqual(find_sum(-1, -4), -5)

    @unittest.expectedFailure
    def test_sum_negative_check_1(self):
        self.assertNotEqual(find_sum(2, 9), 11)


class TestCalculatorDiff(unittest.TestCase):

    def test_diff_positive_check_1(self):
        self.assertEqual(find_diff(10, 4), 6)

    def test_diff_positive_check_2(self):
        self.assertEqual(find_diff(-1, 3), -4)

    @unittest.expectedFailure
    def test_diff_negative_check_1(self):
        self.assertEqual(find_diff(40, 5), 8)


class TestCalculatorDiv(unittest.TestCase):

    def test_div_positive_check_1(self):
        self.assertIsInstance(find_div(7, 2), float)

    def test_div_positive_check_2(self):
        self.assertEqual(find_div(9, 3), 3)

    @unittest.expectedFailure
    def test_div_negative_check_1(self):
        self.assertEqual(find_div(2, 1), 5)

    def test_div_by_zero(self):
        with self.assertRaises(ZeroDivisionError):
            find_div(4, 0)


class TestCalculatorMultiple(unittest.TestCase):

    def test_multiple_positive_check_1(self):
        self.assertIsInstance(find_multiple(7, 2), int)

    def test_multiple_positive_check_2(self):
        self.assertEqual(find_multiple(9, 3), 27)

    @unittest.expectedFailure
    def test_multiple_negative_check_1(self):
        self.assertNotEqual(find_multiple(2, 1), 2)

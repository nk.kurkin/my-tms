"""
Module with functions for different mathematics operations
"""


def find_sum(first_element: int, second_element: int) -> int:
    """
    Finds sum of two provided elements
    :param first_element: first input element
    :param second_element: second input element
    :return: two elements sum
    """
    return first_element + second_element


def find_diff(first_element: int, second_element: int) -> int:
    """
    Finds diff of two provided elements
    :param first_element: first input element
    :param second_element: second input element
    :return: two elements diff
    """
    return first_element - second_element


def find_multiple(first_element: int, second_element: int) -> int:
    """
    Finds multiplication of two provided elements
    :param first_element: first input element
    :param second_element: second input element
    :return: two elements multiplication
    """
    return first_element * second_element


def find_div(first_element: int, second_element: int) -> float:
    """
    Finds division of two provided elements
    :param first_element: first input element
    :param second_element: second input element
    :return: two elements division
    """
    return first_element / second_element

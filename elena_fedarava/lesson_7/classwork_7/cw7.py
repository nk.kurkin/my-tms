import numpy as np

# Task1: Петя перешел в другую школу. На уроке физкультуры ему понадобилось
# определить своё место в строю. Помогите ему это сделать. Программа получает
# на вход не возрастающую последовательность натуральных чисел, означающих рост
# каждого человека в строю. После этого вводится число X – рост Пети.
# Выведите номер, под которым Петя должен встать в строй. Если в строю есть
# люди с одинаковым ростом,таким же, как у Пети, то он должен встать после них.
# rank([165, 163, 162, 160, 157, 157, 155, 154], 162)  #=> 3

# чуть заморочилась над тем, если бы хотела еще и выводить очередь после
# добавления Пети туда


def rank(lst, n):
    """The function calculates a number of Petr's place in a row, shows it"""
    for count, i in enumerate(lst):
        if i < n:
            lst.insert(count, n)
            break
    print('Task 1:', 'Строй:', lst, 'Место Пети в строю:', count + 1)


rank([165, 163, 162, 160, 157, 157, 155, 154], 162)

# Task2: Дан список чисел. Выведите все элементы списка, которые больше
# предыдущего элемента.
# [1, 5, 2, 4, 3]  #=> [5, 4]
# [1, 2, 3, 4, 5] #=> [2, 3, 4, 5]
lst1 = [1, 2, 3, 4, 5]
lst1_new = []
temp = lst1[0]
for i in lst1[1:]:
    if i > temp:
        lst1_new.append(i)
    temp = i
print('Task 2:', lst1_new)

# Task3: Напишите программу, принимающую зубчатый массив любого типа и
# возвращающего его "плоскую" копию.
# List = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]
#  сделала для вложенности не более чем на 3 порядка
lst2 = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]
lst2_flat = []
for i in lst2:
    if isinstance(i, list):
        for k in i:
            if isinstance(k, list):
                for j in k:
                    if isinstance(j, list):
                        for p in j:
                            lst2_flat.append(p)
                    else:
                        lst2_flat.append(j)
            else:
                lst2_flat.append(k)
    else:
        lst2_flat.append(i)

print('Task 3:', lst2_flat)

# Task4: 4. Напишите функцию, которая принимает на вход одномерный массив и два
# числа - размеры выходной матрицы. На выход программа должна подавать матрицу
# нужного размера, сконструированную из элементов массива.
# reshape([1, 2, 3, 4, 5, 6], 2, 3) =>
# [
#     [1, 2, 3],
#     [4, 5, 6]
# ]
# reshape([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2) =>
# [
#     [1, 2],
#     [3, 4],
#     [5, 6],
#     [7, 8]
# ]


def reshape(lst, n, length):
    new_lst = np.reshape(lst, (n, length))
    print('Task 4:\n', new_lst)


reshape([1, 2, 3, 4, 5, 6, 7, 8], 4, 2)

# Task1(Generators): Напишите функцию-генератор, которая вычисляет числа
# фибоначчи.


def fib(n):
    x1, x2 = 0, 1
    for _ in range(n):
        yield x1
        x1, x2 = x2, x1 + x2


fib_list = []
a = fib(10)
for i in a:
    fib_list.append(i)
print('Task 1(Generators):', fib_list)

# Task2(Generators): Напишите генератор списка который принимает список
# numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7] и возвращает
# генератор только с положительными числами
numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
numbers_plus = [x for x in numbers if x > 0]
print('Task 2(Generators):', numbers_plus)

# Task3(Generators): Необходимо составить список чисел которые указывают на
# длину слов в строке: sentence = "the quick brown fox jumps over the lazy dog"
# но только если слово не "the".
sentence = "the quick brown fox jumps over the lazy dog"
sentence_list = sentence.split()
list_sentence_count = [len(x) for x in sentence_list if x != 'the']
print('Task 3(Generators):', list_sentence_count)
